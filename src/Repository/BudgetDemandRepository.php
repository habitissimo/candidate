<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\BudgetDemand;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method BudgetDemand|null find($id, $lockMode = null, $lockVersion = null)
 * @method BudgetDemand|null findOneBy(array $criteria, array $orderBy = null)
 * @method BudgetDemand[]    findAll()
 * @method BudgetDemand[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BudgetDemandRepository extends ServiceEntityRepository
{

    const LIMIT = 5;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BudgetDemand::class);
    }

    public function listPaginatedAll($page = 1, $limit = self::LIMIT, $fetchJoinCollection = false): Paginator
    {
        $query = $this->createQueryBuilder('lp')
                        ->orderBy('lp.id', 'ASC')
                        ->setFirstResult($this->getFromPage($page, $limit))
                        ->setMaxResults($limit)
                        ->getQuery();
            
        return new Paginator($query, $fetchJoinCollection);
    }

    public function listPaginatedByEmail($email, $page = 1, $limit = self::LIMIT, $fetchJoinCollection = false): Paginator
    {
        $query = $this->createQueryBuilder('lp')
                        ->orderBy('lp.id', 'ASC')
                        ->andWhere('lp.email = :email')
                        ->setParameter('email', $email)
                        ->setFirstResult($this->getFromPage($page, $limit))
                        ->setMaxResults($limit)
                        ->getQuery();
            
        return new Paginator($query, $fetchJoinCollection);
    }

    private function getFromPage($page, $limit)
    {
        return ($page - 1) * $limit;
    }

}