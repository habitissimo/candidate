#!/usr/bin/env python

from pyspark.sql import SQLContext
from pyspark import SparkContext
from pyspark.ml.feature import RegexTokenizer, remover, CountVectorizer
from pyspark.ml.classification import LogisticReg
from pyspark.ml import Pipeline
from pyspark.ml.feature import OneHotEncoder, StringIndexer, VectorAssembler
from pyspark.ml.tuning import ParamGridBuilder, CrossValidator

sc = SparkContext()
sql_context = SQLContext(sc)

# Read data from training
data = sql_context.read.options(
    header='true', inferschema='true').load('backend_challenge_training_data.csv')
data_drop_list = ['Categoria']
data = data.select(
    [column for column in data.columns not in words_accepted_drop_list])

# Read accepted words
words_accepted = sql_context.read.options(
    header='true', inferschema='true').load('habitissimoAcceptedWords.csv')
words_accepted_drop_list = ['category', 'score']
words_accepted = data.select(
    [column for column in data.columns if column not in words_accepted_drop_list])

# Set Array of unaccepted words. AKA stop words
set_words_accepted = set(words_accepted)
descriptionWords = descriptionRequest.lower().split()
stopWords = descriptionRequest.lower().split()

stopWords.show(10)

# Tokenize the description column
tokenizer = RegexTokenizer(
    inputCol="Description", outputCol="DescriptionTokenizer", pattern="\\W")
# Remove words
remover = StopWordsRemover(
    inputCol="DescriptionTokenizer", outputCol="DescriptionFiltered").setStopWords(stopWords)
# Get vectors
vectors = CountVectorizer(
    inputCol="DescriptionFiltered", outputCol="DescriptionFeatures", vocabSize=10000, minDF=5)
# Index the return values
labelIdx = StringIndexer(inputCol="Category", outputCol="Label")

pipeline = Pipeline(stages=[tokenizer, remover, vectors, labelIdx])

pipelineFit = pipeline.fit(data)

dataset = pipelineFit.transform(data)

# Cross-Validation
(trainingData, testData) = dataset.randomSplit([0.7, 0.3], seed=3)

logisticReg = LogisticRegression(maxIter=20, regParam=0.3, elasticNetParam=0)

paramGrid = (ParamGridBuilder()
             .addGrid(logisticReg.regParam, [0.1, 0.3, 0.5])
             .addGrid(logisticReg.elasticNetParam, [0.0, 0.1, 0.2])
             .build())

evaluator = MulticlassClassificationEvaluator(predictionCol="prediction")

crossValidation = CrossValidator(estimator=logisticReg,
                                 estimatorParamMaps=paramGrid,
                                 evaluator=evaluator,
                                 numFolds=3)
crossValidationModel = crossValidation.fit(trainingData)

predictions = crossValidationModel.transform(testData)

evaluator.evaluate(predictions)
