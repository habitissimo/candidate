<?php

declare(strict_types=1);

namespace App\Controller;

use \Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use \App\Service\RequestService;
use \App\Service\UserBudgetDemandService;
use \App\Service\BudgetDemandService;
use \App\Service\ResponseService;

/**
 * Class BudgetDemandController
 * @package App\Controller
 */
class BudgetDemandController extends Controller
{

    private $requestService;
    private $userBudgetDemandService;

    public function __construct(
        RequestService $requestService,
        UserBudgetDemandService $userBudgetDemandService,
        BudgetDemandService $budgetDemandService,
        ResponseService $responseService
    ) {
        $this->requestService = $requestService;
        $this->userBudgetDemandService = $userBudgetDemandService;
        $this->budgetDemandService = $budgetDemandService;
        $this->responseService = $responseService;
    }

    /**
     * @param Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/budget/demand/create", name="budget_demand_create")
     * @Method("POST")
     */
    public function budgetDemandCreate(Request $request)
    {
        $userBudgetDemand = $this->requestService->getUserBudgetDemandCreate($request);
        $response = $this->userBudgetDemandService->userCreateOrUpdateAndBudgetDemandCreate($userBudgetDemand);
        return $this->responseService->getResponse($response);
    }

    /**
     * @param Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/budget/demand/update", name="budget_demand_update")
     * @Method("POST")
     */
    public function budgetDemandUpdate(Request $request)
    {
        $budgetDemand = $this->requestService->getBudgetDemandUpdate($request);
        $response = $this->budgetDemandService->update($budgetDemand);
        return $this->responseService->getResponse($response);
    }

    /**
     * @param Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/budget/demand/publish", name="budget_demand_publish")
     * @Method("POST")
     */
    public function budgetDemandPublish(Request $request)
    {
        $budgetDemand = $this->requestService->getBudgetDemandUpdate($request);
        $response = $this->budgetDemandService->publish($budgetDemand);
        return $this->responseService->getResponse($response);
    }

    /**
     * @param Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/budget/demand/discard", name="budget_demand_discard")
     * @Method("DELETE")
     */
    public function budgetDemandDiscard(Request $request)
    {
        $budgetDemand = $this->requestService->getBudgetDemandUpdate($request);
        $response = $this->budgetDemandService->discard($budgetDemand);
        return $this->responseService->getResponse($response);
    }

    /**
     * @param Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/budget/demand/list/{page}/{email}", name="budget_demand_list", defaults={"email"=null})
     * @Method("GET")
     */
    public function budgetDemandList(Request $request)
    {
        $budgetDemand = $this->requestService->getBudgetDemandList($request);
        $response = $this->budgetDemandService->list($budgetDemand, $request->get("page"));

        return $this->responseService->getResponse($response);
    }

}
