<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class UserBudgetDemandCreateRequestModel
{
    use Feature\Required\DescriptionTrait;
    use Feature\Required\EmailTrait;
    use Feature\Required\TelephoneTrait;
    use Feature\Required\AddressTrait;
    use Feature\Optional\TitleTrait;
    use Feature\Optional\CategoryTrait;
}
