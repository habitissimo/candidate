<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class UserRequestModel
{
    use Feature\Required\TelephoneTrait;
    use Feature\Required\AddressTrait;
    use Feature\Required\EmailTrait;
}