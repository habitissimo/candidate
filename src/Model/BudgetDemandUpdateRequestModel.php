<?php

namespace App\Model;

class BudgetDemandUpdateRequestModel
{
    use Feature\Required\IdTrait;
    use Feature\Optional\TitleTrait;
    use Feature\Optional\DescriptionTrait;
    use Feature\Optional\CategoryTrait;
}
