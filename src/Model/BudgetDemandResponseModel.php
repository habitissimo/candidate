<?php

namespace App\Model;

class BudgetDemandResponseModel
{
    use Feature\Required\IdTrait;
    use Feature\SuccessTrait;
    use Feature\ErrorTrait;
}