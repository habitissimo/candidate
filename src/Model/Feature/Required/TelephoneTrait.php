<?php

declare(strict_types=1);

namespace App\Model\Feature\Required;

use Symfony\Component\Validator\Constraints as Assert;

Trait TelephoneTrait
{

    /**
     * @Assert\Type("string")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 7,
     *      max = 20,
     *      minMessage = "Your telephone must be at least {{ limit }} characters long",
     *      maxMessage = "Your telephone cannot be longer than {{ limit }} characters"
     * )
     */
    private $telephone;

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone($telephone): self
    {
        $this->telephone = $telephone;
        return $this;
    }

}