<?php

declare(strict_types=1);

namespace App\Model\Feature\Required;

use Symfony\Component\Validator\Constraints as Assert;

Trait DescriptionTrait
{

    /**
     * @Assert\Type("string")
     * @Assert\NotNull()
     */
    private $description;

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;
        return $this;
    }

}