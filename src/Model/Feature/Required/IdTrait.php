<?php

declare(strict_types=1);

namespace App\Model\Feature\Required;

use Symfony\Component\Validator\Constraints as Assert;

Trait IdTrait
{

    /**
     * @Assert\Type("integer")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

}