<?php

declare(strict_types=1);

namespace App\Model\Feature\Required;

use Symfony\Component\Validator\Constraints as Assert;

Trait AddressTrait
{

    /**
     * @Assert\Type("string")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Your address must be at least {{ limit }} characters long",
     *      maxMessage = "Your address cannot be longer than {{ limit }} characters"
     * )
     */
    private $address;

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address): self
    {
        $this->address = $address;
        return $this;
    }

}