<?php

declare(strict_types=1);

namespace App\Model\Feature\Required;

use Symfony\Component\Validator\Constraints as Assert;

Trait EmailTrait
{

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Email(message = "The email '{{ value }}' is not a valid email.")
     */
    private $email;
    
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): self
    {
        $this->email = $email;
        return $this;
    }

}