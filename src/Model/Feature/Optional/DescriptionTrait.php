<?php

declare(strict_types=1);

namespace App\Model\Feature\Optional;

use Symfony\Component\Validator\Constraints as Assert;

Trait DescriptionTrait
{

    /**
     * @Assert\Type("string")
     */
    private $description;

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;
        return $this;
    }

}