<?php

declare(strict_types=1);

namespace App\Model\Feature\Optional;

use Symfony\Component\Validator\Constraints as Assert;

Trait CategoryTrait
{

    /**
     * @Assert\Type("string")
     */
    private $category;

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category): self
    {
        $this->category = $category;
        return $this;
    }

}