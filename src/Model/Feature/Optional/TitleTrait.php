<?php

declare(strict_types=1);

namespace App\Model\Feature\Optional;

use Symfony\Component\Validator\Constraints as Assert;

Trait TitleTrait
{

    /**
     * @Assert\Type("string")
     */
    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title): self
    {
        $this->title = $title;
        return $this;
    }

}