<?php

declare(strict_types=1);

namespace App\Model\Feature;

use Symfony\Component\Validator\Constraints as Assert;

trait ErrorTrait
{

    /**
     * @Assert\Type("string")
     */
    private $error;

    public function getError()
    {
        return $this->error;
    }

    public function setError($error): self
    {
        $this->error = $error;
        return $this;
    }

}