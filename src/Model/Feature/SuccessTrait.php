<?php

declare(strict_types=1);

namespace App\Model\Feature;

use Symfony\Component\Validator\Constraints as Assert;

trait SuccessTrait
{

    /**
     * @Assert\Type("boolean")
     */
    private $success = false;

    public function getSuccess()
    {
        return $this->success;
    }

    public function setSuccess($success): self
    {
        $this->success = !!$success;
        return $this;
    }

}