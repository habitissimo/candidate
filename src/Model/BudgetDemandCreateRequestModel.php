<?php

namespace App\Model;

class BudgetDemandCreateRequestModel
{
    use Feature\Required\EmailTrait;
    use Feature\Required\DescriptionTrait;
    use Feature\Optional\TitleTrait;
    use Feature\Optional\CategoryTrait;
}