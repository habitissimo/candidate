<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class UserResponseModel
{
    use Feature\Required\IdTrait;
    use Feature\SuccessTrait;
    use Feature\ErrorTrait;
}