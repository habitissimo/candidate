<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

use \App\Model\UserResponseModel;
use \App\Model\BudgetDemandResponseModel;

class UserBudgetDemandResponseModel
{

    use Feature\SuccessTrait;

    /**
     * @Assert\Type("\App\Model\UserResponseModel")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Valid
     */
    private $user;

    /**
     * @Assert\Type("\App\Model\BudgetDemandResponseModel")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Valid
     */
    private $budgetDemand;

    public function getUser(): ?UserResponseModel
    {
        return $this->user;
    }

    public function setUser(UserResponseModel $userResponseModel): self
    {
        $this->user = $userResponseModel;
        return $this;
    }

    public function getBudgetDemand(): ?BudgetDemandResponseModel
    {
        return $this->budgetDemand;
    }

    public function setBudgetDemand(BudgetDemandResponseModel $budgetDemandResponseModel): self
    {
        $this->budgetDemand = $budgetDemandResponseModel;
        return $this;
    }

}
