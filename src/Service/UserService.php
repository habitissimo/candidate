<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

use Doctrine\ORM\EntityManagerInterface;

use \App\Model\UserRequestModel;
use \App\Entity\User;
use \App\Model\UserResponseModel;

class UserService
{

    private $entityManager;
    private $serializerService;
    private $userResponseModel;
    
    public function __construct(
        EntityManagerInterface $entityManager,
        SerializerInterface $serializerService,
        UserResponseModel $userResponseModel
    ) {
        $this->entityManager = $entityManager;
        $this->serializerService = $serializerService;
        $this->userResponseModel = $userResponseModel;
    }

    public function createOrUpdate(UserRequestModel $userRequestModel): UserResponseModel
    {
        $userEntityNew = $this->getEntity($userRequestModel);
        $userEntity = $this->createOrUpdateStrategy($userEntityNew);
        try{
            $this->persist($userEntity);
            $this->setSuccess($userEntity);
        } catch(\Exception $e) {
            // TODO: Log the exception
            $this->setFailure($e->getMessage());
        }
        return $this->userResponseModel;
    }

    private function getEntity(UserRequestModel $userRequestModel): User
    {
        return $this->serializerService->deserialize(
            $this->serializerService->serialize($userRequestModel, 'json'), 
            User::class,
            'json'
        );
    }

    private function createOrUpdateStrategy(User $userEntityNew): User
    {
        $userEntityFound = $this->entityManager
                            ->getRepository('\App\Entity\User')
                            ->findOneBy(['email' =>  $userEntityNew->getEmail()]);

        return $userEntityFound
                ? $userEntityFound->setTelephone( trim($userEntityNew->getTelephone()) )->setAddress( $userEntityNew->getAddress() )
                : $userEntityNew;
    }

    private function persist(User $userEntity): void
    {
        $this->entityManager->persist($userEntity);
        $this->entityManager->flush();
    }

    private function setSuccess(User $userEntity)
    {
        $this->userResponseModel
                ->setId($userEntity->getId())
                ->setError(null)
                ->setSuccess(true);
    }

    private function setFailure(String $error)
    {
        $this->userResponseModel
                ->setId(null)
                ->setError($error)
                ->setSuccess(false);
    }

}