<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\SerializerInterface;

use \App\Model\UserBudgetDemandCreateRequestModel;
use \App\Model\BudgetDemandUpdateRequestModel;
use \App\Model\BudgetDemandListRequestModel;

class RequestService
{

    private $serializer;
    
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    public function getUserBudgetDemandCreate(Request $request): UserBudgetDemandCreateRequestModel
    {
        return $this->getModel($request->getContent(), UserBudgetDemandCreateRequestModel::class);
    }

    public function getBudgetDemandUpdate(Request $request): BudgetDemandUpdateRequestModel
    {
        return $this->getModel($request->getContent(), BudgetDemandUpdateRequestModel::class);
    }

    public function getBudgetDemandList(Request $request): ?BudgetDemandListRequestModel
    {
        $email = $request->get("email");
        if($email) {
            $dummy = new \StdClass();
            $dummy->email = $email;
            return $this->getModel(json_encode($dummy), BudgetDemandListRequestModel::class);
        }
        return null;
    }
    
    private function getModel(string $request, string $clazz)
    {
        return $this->serializer->deserialize($request, $clazz, 'json', array('allow_extra_attributes' => false));
    }

}
