<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidateService
{

    private $validator;

    private $errors;
    
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate($object)
    {
        $this->errors = $this->validator->validate($object);
        if($this->hasErrors()) {
            // TODO: Log the exception and handle it
            throw new \Exception($this->errors);
        }
        return $object;
    }

    private function hasErrors()
    {
        return (Boolean)(0 < count($this->errors));
    }

}