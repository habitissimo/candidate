<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Serializer\SerializerInterface;

class ResponseService
{
    private $serializer;
    
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function getResponse($obj): Response
    {
        return new Response($this->serializer->serialize($obj, 'json'));
    }
}
