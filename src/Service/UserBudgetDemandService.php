<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

use \App\Model\UserBudgetDemandCreateRequestModel;
use \App\Model\UserRequestModel;
use \App\Model\UserResponseModel;
use \App\Model\BudgetDemandCreateRequestModel;
use \App\Model\BudgetDemandResponseModel;
use \App\Model\UserBudgetDemandResponseModel;

class UserBudgetDemandService
{

    private $userService;
    private $budgetDemandService;
    private $userRequestModel;
    private $budgetDemandCreateRequestModel;
    private $userBudgetDemandResponseModel;
    
    public function __construct(
        UserService $userService,
        BudgetDemandService $budgetDemandService,
        UserRequestModel $userRequestModel,
        BudgetDemandCreateRequestModel $budgetDemandCreateRequestModel,
        UserBudgetDemandResponseModel $userBudgetDemandResponseModel
    ) {
        $this->userService = $userService;
        $this->budgetDemandService = $budgetDemandService;
        $this->userRequestModel = $userRequestModel;
        $this->budgetDemandCreateRequestModel = $budgetDemandCreateRequestModel;
        $this->userBudgetDemandResponseModel = $userBudgetDemandResponseModel;
    }

    public function userCreateOrUpdateAndBudgetDemandCreate(
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel
    ): UserBudgetDemandResponseModel
    {
        $this->setUserModel($userBudgetDemandCreateRequestModel);
        $this->setBudgetDemandCreateModel($userBudgetDemandCreateRequestModel);
        $userResponse = $this->userService->createOrUpdate($this->userRequestModel);
        $budgetResponse = $this->budgetDemandService->create($this->budgetDemandCreateRequestModel);
        $this->setUserBudgetDemandResponseModel($userResponse, $budgetResponse);
        
        return $this->userBudgetDemandResponseModel;
    }

    private function setUserModel(UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel): void
    {
        $this->userRequestModel
                ->setEmail($userBudgetDemandCreateRequestModel->getEmail())
                ->setTelephone($userBudgetDemandCreateRequestModel->getTelephone())
                ->setAddress($userBudgetDemandCreateRequestModel->getAddress());
    }

    private function setBudgetDemandCreateModel(UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel): void
    {
        $this->budgetDemandCreateRequestModel
                ->setEmail($userBudgetDemandCreateRequestModel->getEmail())
                ->setTitle($userBudgetDemandCreateRequestModel->getTitle())
                ->setDescription($userBudgetDemandCreateRequestModel->getDescription())
                ->setCategory($userBudgetDemandCreateRequestModel->getCategory());
    }

    private function setUserBudgetDemandResponseModel(
        UserResponseModel $userResponseModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    ): void
    {
        $this->userBudgetDemandResponseModel
                ->setSuccess($this->isSuccess($userResponseModel, $budgetDemandResponseModel))
                ->setUser($userResponseModel)
                ->setBudgetDemand($budgetDemandResponseModel);
    }

    private function isSuccess(
        UserResponseModel $UserResponseModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        return $UserResponseModel->getSuccess() && $budgetDemandResponseModel->getSuccess();
    }

}