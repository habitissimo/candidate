<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

use Doctrine\ORM\EntityManagerInterface;

use \App\Model\BudgetDemandCreateRequestModel;
use \App\Model\BudgetDemandUpdateRequestModel;
use \App\Model\BudgetDemandListRequestModel;
use \App\Model\BudgetDemandResponseModel;
use \App\Entity\BudgetDemand;
use \App\Service\ValidateService;

class BudgetDemandService
{

    const BUDGET_DEMAND_WAS_NOT_FOUND = "Your budget demand was not found with id: ";
    const BUDGET_DEMAND_CAN_NOT_BE_MODIFIED = "Your budget demand can not be modified";
    const BUDGET_DEMAND_CAN_NOT_BE_PUBLISH_DUE_TO_TITLE = "Your budget demand can not be published, title is not set";
    const BUDGET_DEMAND_CAN_NOT_BE_PUBLISH_DUE_TO_CATEGORY = "Your budget demand can not be published, category is not set";
    const BUDGET_DEMAND_ALREADY_DISCARDED = "Your budget demand is already discarded";
    const PENDING_STATUS = "Pending";
    const PUBLISHED_STATUS = "Published";
    const DISCARDED_STATUS = "Discarded";

    private $entityManager;
    private $validateService;
    private $serializerService;
    private $budgetDemandResponseModel;

    public function __construct(
        EntityManagerInterface $entityManager,
        ValidateService $validateService,
        SerializerInterface $serializerService,
        BudgetDemandResponseModel $budgetDemandResponseModel
    ) {
        $this->entityManager = $entityManager;
        $this->validateService = $validateService;
        $this->serializerService = $serializerService;
        $this->budgetDemandResponseModel = $budgetDemandResponseModel;
    }

    public function create(BudgetDemandCreateRequestModel $budgetDemandCreateRequestModel): BudgetDemandResponseModel
    {
        $budgetDemandEntity = $this->serializerService
                                        ->deserialize(
                                            $this->serializerService->serialize($budgetDemandCreateRequestModel, 'json'), 
                                            BudgetDemand::class, 
                                            'json'
                                        );

        $this->persist($budgetDemandEntity);
        return $this->budgetDemandResponseModel;
    }

    public function update(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel): BudgetDemandResponseModel
    {
        $budgetDemandEntity = $this->getEntityValidated($budgetDemandUpdateRequestModel);

        if($this->isBudgetDemandNull($budgetDemandEntity, $budgetDemandUpdateRequestModel)) {
            return $this->budgetDemandResponseModel;
        }
        if($this->isBudgetDemandStatusNotPending($budgetDemandEntity, $budgetDemandUpdateRequestModel)) {
            return $this->budgetDemandResponseModel;
        }
        $description = (!$budgetDemandUpdateRequestModel->getDescription())
                            ? $budgetDemandEntity->getDescription()
                            : $budgetDemandUpdateRequestModel->getDescription();
        $budgetDemandEntity->setTitle($budgetDemandUpdateRequestModel->getTitle())
                                ->setDescription($description)
                                ->setCategory($budgetDemandUpdateRequestModel->getCategory());

        $this->persist($budgetDemandEntity);
        return $this->budgetDemandResponseModel;
    }

    public function publish(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel): BudgetDemandResponseModel
    {
        $budgetDemandEntity = $this->getEntityValidated($budgetDemandUpdateRequestModel);

        if($this->isBudgetDemandNull($budgetDemandEntity, $budgetDemandUpdateRequestModel)) {
            return $this->budgetDemandResponseModel;
        }
        if($this->isBudgetDemandStatusNotPending($budgetDemandEntity, $budgetDemandUpdateRequestModel)) {
            return $this->budgetDemandResponseModel;
        }
        if(!$budgetDemandEntity->getTitle() || strlen(trim($budgetDemandEntity->getTitle())) === 0) {
            $this->setFailure(self::BUDGET_DEMAND_CAN_NOT_BE_PUBLISH_DUE_TO_TITLE);
            return $this->budgetDemandResponseModel;
        }
        if(!$budgetDemandEntity->getCategory() || strlen(trim($budgetDemandEntity->getCategory())) === 0) {
            $this->setFailure(self::BUDGET_DEMAND_CAN_NOT_BE_PUBLISH_DUE_TO_CATEGORY);
            return $this->budgetDemandResponseModel;
        }
        $budgetDemandEntity->setStatus(self::PUBLISHED_STATUS);

        $this->persist($budgetDemandEntity);
        return $this->budgetDemandResponseModel;
    }

    public function discard(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel): BudgetDemandResponseModel
    {
        $budgetDemandEntity = $this->getEntityValidated($budgetDemandUpdateRequestModel);

        if($this->isBudgetDemandNull($budgetDemandEntity, $budgetDemandUpdateRequestModel)) {
            return $this->budgetDemandResponseModel;
        }
        if($budgetDemandEntity->getStatus() == self::DISCARDED_STATUS) {
            $this->setFailure(self::BUDGET_DEMAND_ALREADY_DISCARDED);
            return $this->budgetDemandResponseModel;
        }

        $budgetDemandEntity->setStatus(self::DISCARDED_STATUS);

        $this->persist($budgetDemandEntity);
        return $this->budgetDemandResponseModel;
    }

    public function list(?BudgetDemandListRequestModel $budgetDemandListRequestModel, $page = 1): \ArrayIterator
    {
        if($budgetDemandListRequestModel instanceof BudgetDemandListRequestModel) {
            $budgetDemandListRequestModel = $this->validateService->validate($budgetDemandListRequestModel);
            return $this->entityManager
                        ->getRepository('\App\Entity\BudgetDemand')
                        ->listPaginatedByEmail($budgetDemandListRequestModel->getEmail(), $page)
                        ->getIterator();
        }

        return $this->entityManager
                    ->getRepository('\App\Entity\BudgetDemand')
                    ->listPaginatedAll($page)
                    ->getIterator();
    }

    private function getEntityValidated(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel): ?BudgetDemand
    {
        $budgetDemandUpdateRequestModel = $this->validateService->validate($budgetDemandUpdateRequestModel);
        return $this->findById($budgetDemandUpdateRequestModel->getId());
    }

    private function isBudgetDemandNull(?BudgetDemand $budgetDemandEntity, BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        if(!$budgetDemandEntity) {
            $this->setFailure(self::BUDGET_DEMAND_WAS_NOT_FOUND . $budgetDemandUpdateRequestModel->getId());
            return true;
        }
        return false;
    }

    private function isBudgetDemandStatusNotPending(?BudgetDemand $budgetDemandEntity, BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        if($budgetDemandEntity->getStatus() != self::PENDING_STATUS) {
            $this->setFailure(self::BUDGET_DEMAND_CAN_NOT_BE_MODIFIED);
            return true;
        }
        return false;
    }

    private function findById($id): ?BudgetDemand
    {
        return $this->entityManager
                    ->getRepository('\App\Entity\BudgetDemand')
                    ->find($id);
    }

    /**
     * @return BudgetDemand[]
     */
    private function findByEmail($email): ?Array
    {
        return $this->entityManager
                    ->getRepository('\App\Entity\BudgetDemand')
                    ->findBy(['email' =>  $email]);
    }

    private function persist(BudgetDemand $budgetDemandEntity): void
    {
        try{
            $this->entityManager->persist($budgetDemandEntity);
            $this->entityManager->flush();
            $this->setSuccess($budgetDemandEntity);
        } catch(\Exception $e) {
            // TODO: Log the exception
            $this->setFailure($e->getMessage());
        }
    }

    private function setSuccess(BudgetDemand $budgetDemandEntity)
    {
        $this->budgetDemandResponseModel
                ->setId($budgetDemandEntity->getId())
                ->setError(null)
                ->setSuccess(true);
    }

    private function setFailure(String $error)
    {
        $this->budgetDemandResponseModel
                ->setError($error)
                ->setSuccess(false);
    }

}