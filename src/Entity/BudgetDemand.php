<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BudgetDemandRepository")
 * @ORM\Table(indexes={ 
 *      @ORM\Index(columns={"user"}),
 *      @ORM\Index(columns={"status"})
 * })
 */
class BudgetDemand
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $category;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('Pending', 'Published', 'Discarded') NOT NULL DEFAULT 'Pending'")
     */
    private $status = 'Pending';

    /**
     * @ORM\Column(type="string", name="user", length=100)
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="email")
     * @ORM\JoinColumn(name="user_budget_demand", referencedColumnName="email", onDelete="CASCADE")
     */
    private $email;

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

}
