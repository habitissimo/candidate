Habitissimo backend challenge
=============================

**Technical**

  * PHP 7.1
  * MySql server 5.7
  * Composer
  * Symfony 4 (See composer.json)

**Instalation**

  * Once you have a copy of this challenge, please be sure you have composer installed
  * Get a terminal and locate into the project folder
  * Once you get composer installed on your machine, please type `**> composer install**`
  * Be sure you have a **MySql server running** and is configured on default `**Port: 3306**`, `**User: root**`, `**Password: <empty>**`
  * If you Database requires password, then edit the file .env at project root folder `**DATABASE_URL=mysql://root:PASSWORD@127.0.0.1:3306/habitissimo_backend_challenge**`
  * Create a Database with name **habitissimo_backend_challenge**
    * PhpMyAdmin: `**habitissimo_backend_challenge.sql**`, at root folder, will help you importing the Database
    * Doctrine:
      * First, create the database. `**> php bin/console doctrine:database:create**`
      * And then, migrate it `**> php bin/console doctrine:migrations:migrate**`
      * You have Version20180410100945.php already generated on **/Migrations** folder

**Run**

  * Go to the terminal again and type, `**> php bin/console server:run 127.0.0.1:8200**`
  * It will run a simple server that is listening at `**http://127.0.0.1:8200**`
  * This port setting is due to the `**Habitissimo.postman_collection.json**`, the has the requests to play with the API
  * Feel free to change it when you import the collections (Remember to sync it with the same port on the server)

**Unit Testing**

  * Go to the terminal and type `**> php bin/phpunit**`

**Functional Testing**

  * Import the file `**Habitissimo.postman_collection.json**` into your postman and feel free to play with the requests
  * There are 6 requests which you can play with:
    * `BackendChallengeController: budget_demand_create` (Create new budget demand and create or update an user)
    * `BackendChallengeController: budget_demand_update` (Update a budget demand)
    * `BackendChallengeController: budget_demand_publish` (Publish a budget demand which status is pending)
    * `BackendChallengeController: budget_demand_discard` (Discard a budget demand which status is not discarded)
    * `BackendChallengeController: budget_demand_list` (List all the budgets demands paginated)
    * `BackendChallengeController: budget_demand_list_with_email` (List all the budget demands paginated from an email given)

**Optional: Category suggested**

  * There is a POC into the folder AI in order to show how the `**Deep learning neural network**` should goes
  * This task has not been tested and either proved. It's only for reference about how to.

Enjoy :)

> Victor Javier Sanchez Jimenez.

> Mobile: 693 85 30 72
