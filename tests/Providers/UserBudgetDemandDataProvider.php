<?php

declare(strict_types=1);

namespace App\Tests\Providers;

use \App\Model\UserBudgetDemandCreateRequestModel;

class UserBudgetDemandDataProvider
{

    private const DEFAULT_TITLE = "Whatever title";
    private const DEFAULT_CATEGORY = "Whatever category";
    private const DEFAULT_DESCRIPTION = "A sort Description";
    private const DEFAULT_EMAIL = "an_email@server.com";
    private const DEFAULT_TELEPHONE = "0034 956 32 54 85";
    private const DEFAULT_ADDRESS = "Whatever St. N24";

    public function validUserBudgetDemandCreateRequestModel()
    {
        return [
            [
                (new UserBudgetDemandCreateRequestModel())
                        ->setTitle(self::DEFAULT_TITLE)
                        ->setCategory(self::DEFAULT_CATEGORY)
                        ->setDescription(self::DEFAULT_DESCRIPTION)
                        ->setEmail(self::DEFAULT_EMAIL)
                        ->setTelephone(self::DEFAULT_TELEPHONE)
                        ->setAddress(self::DEFAULT_ADDRESS)
            ],
            [
                (new UserBudgetDemandCreateRequestModel())
                        ->setCategory(self::DEFAULT_CATEGORY)
                        ->setDescription(self::DEFAULT_DESCRIPTION)
                        ->setEmail(self::DEFAULT_EMAIL)
                        ->setTelephone(self::DEFAULT_TELEPHONE)
                        ->setAddress(self::DEFAULT_ADDRESS)
            ],
            [
                (new UserBudgetDemandCreateRequestModel())
                        ->setTitle(self::DEFAULT_TITLE)
                        ->setDescription(self::DEFAULT_DESCRIPTION)
                        ->setEmail(self::DEFAULT_EMAIL)
                        ->setTelephone(self::DEFAULT_TELEPHONE)
                        ->setAddress(self::DEFAULT_ADDRESS)
            ],
            [
                (new UserBudgetDemandCreateRequestModel())
                        ->setDescription(self::DEFAULT_DESCRIPTION)
                        ->setEmail(self::DEFAULT_EMAIL)
                        ->setTelephone(self::DEFAULT_TELEPHONE)
                        ->setAddress(self::DEFAULT_ADDRESS)
            ]
        ];
    }

    public function invalidUserRequestModel()
    {
        return [
            [
                (new UserBudgetDemandCreateRequestModel())
                        ->setTitle(self::DEFAULT_TITLE)
                        ->setCategory(self::DEFAULT_CATEGORY)
                        ->setDescription(self::DEFAULT_DESCRIPTION)
                        ->setTelephone(self::DEFAULT_TELEPHONE)
                        ->setAddress(self::DEFAULT_ADDRESS)
            ],
            [
                (new UserBudgetDemandCreateRequestModel())
                        ->setTitle(self::DEFAULT_TITLE)
                        ->setCategory(self::DEFAULT_CATEGORY)
                        ->setDescription(self::DEFAULT_DESCRIPTION)
                        ->setEmail(self::DEFAULT_EMAIL)
                        ->setAddress(self::DEFAULT_ADDRESS)
            ],
            [
                (new UserBudgetDemandCreateRequestModel())
                        ->setTitle(self::DEFAULT_TITLE)
                        ->setCategory(self::DEFAULT_CATEGORY)
                        ->setDescription(self::DEFAULT_DESCRIPTION)
                        ->setEmail(self::DEFAULT_EMAIL)
                        ->setTelephone(self::DEFAULT_TELEPHONE)
            ],
            [
                (new UserBudgetDemandCreateRequestModel())
                        ->setTitle(self::DEFAULT_TITLE)
                        ->setCategory(self::DEFAULT_CATEGORY)
                        ->setDescription(self::DEFAULT_DESCRIPTION)
                        ->setTelephone(self::DEFAULT_TELEPHONE)
            ],
            [
                (new UserBudgetDemandCreateRequestModel())
                        ->setTitle(self::DEFAULT_TITLE)
                        ->setCategory(self::DEFAULT_CATEGORY)
                        ->setDescription(self::DEFAULT_DESCRIPTION)
                        ->setEmail(self::DEFAULT_EMAIL)
            ]
        ];
    }

    public function invalidBudgetDemandCreateRequestModel()
    {
        return [
            [
                (new UserBudgetDemandCreateRequestModel())
                        ->setTitle(self::DEFAULT_TITLE)
                        ->setCategory(self::DEFAULT_CATEGORY)
                        ->setEmail(self::DEFAULT_EMAIL)
                        ->setTelephone(self::DEFAULT_TELEPHONE)
                        ->setAddress(self::DEFAULT_ADDRESS)
            ]
        ];
    }

}
