<?php

declare(strict_types=1);

namespace App\Tests\Providers\Feature\Optional;

class DescriptionDataProvider
{
    use \App\Tests\Providers\StringDataProvider;
}
