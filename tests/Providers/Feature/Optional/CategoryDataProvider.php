<?php

declare(strict_types=1);

namespace App\Tests\Providers\Feature\Optional;

class CategoryDataProvider
{
    use \App\Tests\Providers\StringDataProvider;
}
