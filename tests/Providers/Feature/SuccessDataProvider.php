<?php

declare(strict_types=1);

namespace App\Tests\Providers\Feature;

class SuccessDataProvider
{
    use \App\Tests\Providers\StringDataProvider;
}
