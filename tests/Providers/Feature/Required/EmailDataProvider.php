<?php

declare(strict_types=1);

namespace App\Tests\Providers\Feature\Required;

class EmailDataProvider
{
    use \App\Tests\Providers\StringDataProvider;
}
