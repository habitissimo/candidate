<?php

declare(strict_types=1);

namespace App\Tests\Providers\Feature\Required;

class IdDataProvider
{
    use \App\Tests\Providers\IntegerDataProvider;
}
