<?php

declare(strict_types=1);

namespace App\Tests\Providers\Feature\Required;

class DescriptionDataProvider
{
    use \App\Tests\Providers\StringDataProvider;
}
