<?php

declare(strict_types=1);

namespace App\Tests\Providers;

use \App\Model\BudgetDemandCreateRequestModel;
use \App\Model\BudgetDemandUpdateRequestModel;
use \App\Model\BudgetDemandListRequestModel;

class ValidateDataProvider
{
    public function validObjects()
    {
        return [
            [new BudgetDemandCreateRequestModel()],
            [new BudgetDemandUpdateRequestModel()],
            [new BudgetDemandListRequestModel()]
        ];
    }

    public function invalidObjects()
    {
        return [
            [new \StdClass()]
        ];
    }
}
