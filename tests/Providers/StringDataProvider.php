<?php

declare(strict_types=1);

namespace App\Tests\Providers;

Trait StringDataProvider
{
    public function validStrings()
    {
        return [
            [""],
            [''],
            ["zzvv3435"],
        ];
    }
}