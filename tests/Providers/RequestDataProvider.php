<?php

declare(strict_types=1);

namespace App\Tests\Providers;

use Symfony\Component\HttpFoundation\Request;

use \App\Model\UserBudgetDemandCreateRequestModel;
use \App\Model\BudgetDemandUpdateRequestModel;
use \App\Model\BudgetDemandListRequestModel;

class RequestDataProvider
{

    private const DEFAULT_ID = 1;
    private const DEFAULT_DESCRIPTION = "A description";
    private const DEFAULT_EMAIL = "whatever@email.com";
    private const DEFAULT_TELEPHONE = "0034 565 32 26 69";
    private const DEFAULT_ADDRESS = "Whatever St. N34";
    private const DEFAULT_TITLE = "Whatever title";
    private const DEFAULT_CATEGORY = "Whatever category";
    private const CLASS_TO_THROW_EXCEPTION_ON_DESERIALIZATION = "ClassToThrowExceptionOnDeserialization";
    

    public function validUserBudgetDemandCreateRequestModel()
    {
        return [
            [
                $this->getUserBudgetDemandCreateRequestModel(),
                '{ 
                    "title": "' . self::DEFAULT_TITLE . '",
                    "description": "' . self::DEFAULT_DESCRIPTION . '",
                    "category": "' . self::DEFAULT_CATEGORY . '",
                    "email": "' . self::DEFAULT_EMAIL . '",
                    "telephone": "' . self::DEFAULT_TELEPHONE . '",
                    "address": "' . self::DEFAULT_ADDRESS . '"
                }'
            ],
            [
                $this->getUserBudgetDemandCreateRequestModel(),
                '{ 
                    "title": null,
                    "description": "' . self::DEFAULT_DESCRIPTION . '",
                    "category": "' . self::DEFAULT_CATEGORY . '",
                    "email": "' . self::DEFAULT_EMAIL . '",
                    "telephone": "' . self::DEFAULT_TELEPHONE . '",
                    "address": "' . self::DEFAULT_ADDRESS . '"
                }'
            ],
            [
                $this->getUserBudgetDemandCreateRequestModel(),
                '{ 
                    "title": "' . self::DEFAULT_TITLE . '",
                    "description": "' . self::DEFAULT_DESCRIPTION . '",
                    "category": null,
                    "email": "' . self::DEFAULT_EMAIL . '",
                    "telephone": "' . self::DEFAULT_TELEPHONE . '",
                    "address": "' . self::DEFAULT_ADDRESS . '"
                }'
            ],
            [
                $this->getUserBudgetDemandCreateRequestModel(),
                '{ 
                    "title": null,
                    "description": "' . self::DEFAULT_DESCRIPTION . '",
                    "category": null,
                    "email": "' . self::DEFAULT_EMAIL . '",
                    "telephone": "' . self::DEFAULT_TELEPHONE . '",
                    "address": "' . self::DEFAULT_ADDRESS . '"
                }'
            ]
        ];
    }

    public function validBudgetDemandUpdateRequestModel()
    {
        return [
            [
                $this->getBudgetDemandUpdateRequestModel(),
                '{ 
                    "id": ' . self::DEFAULT_ID . ',
                    "title": "' . self::DEFAULT_TITLE . '",
                    "description": "' . self::DEFAULT_DESCRIPTION . '",
                    "category": "' . self::DEFAULT_CATEGORY . '"
                }'
            ],
            [
                $this->getBudgetDemandUpdateRequestModel(),
                '{ 
                    "id": ' . self::DEFAULT_ID . ',
                    "description": "' . self::DEFAULT_DESCRIPTION . '",
                    "category": "' . self::DEFAULT_CATEGORY . '"
                }'
            ],
            [
                $this->getBudgetDemandUpdateRequestModel(),
                '{ 
                    "id": ' . self::DEFAULT_ID . ',
                    "title": "' . self::DEFAULT_TITLE . '",
                    "category": "' . self::DEFAULT_CATEGORY . '"
                }'
            ],
            [
                $this->getBudgetDemandUpdateRequestModel(),
                '{ 
                    "id": ' . self::DEFAULT_ID . ',
                    "title": "' . self::DEFAULT_TITLE . '",
                    "description": "' . self::DEFAULT_DESCRIPTION . '"
                }'
            ],
            [
                $this->getBudgetDemandUpdateRequestModel(),
                '{ 
                    "id": ' . self::DEFAULT_ID . ',
                    "category": "' . self::DEFAULT_CATEGORY . '"
                }'
            ],
            [
                $this->getBudgetDemandUpdateRequestModel(),
                '{ 
                    "id": ' . self::DEFAULT_ID . ',
                    "description": "' . self::DEFAULT_DESCRIPTION . '"
                }'
            ],
            [
                $this->getBudgetDemandUpdateRequestModel(),
                '{ 
                    "id": ' . self::DEFAULT_ID . ',
                    "title": "' . self::DEFAULT_TITLE . '"
                }'
            ],
            [
                $this->getBudgetDemandUpdateRequestModel(),
                '{ 
                    "id": ' . self::DEFAULT_ID . '
                }'
            ]
        ];
    }

    public function validBudgetDemandListRequestModelEmailSet()
    {
        return [
            [
                $this->getBudgetDemandListRequestModelEmailSet(),
                '{"email":"' . self::DEFAULT_EMAIL . '"}'
            ]
        ];
    }

    public function validBudgetDemandListRequestModelEmailNotSet()
    {
        return [
            [
                $this->getBudgetDemandListRequestModelEmailNotSet(),
                '{}'
            ]
        ];
    }

    public function invalidDeserialization()
    {
        return [
            [
                $this->getUserBudgetDemandCreateRequestModel(),
                self::CLASS_TO_THROW_EXCEPTION_ON_DESERIALIZATION
            ],
            [
                $this->getBudgetDemandUpdateRequestModel(),
                self::CLASS_TO_THROW_EXCEPTION_ON_DESERIALIZATION
            ],
            [
                $this->getBudgetDemandListRequestModelEmailSet(),
                self::CLASS_TO_THROW_EXCEPTION_ON_DESERIALIZATION
            ],
            [
                $this->getBudgetDemandListRequestModelEmailNotSet(),
                self::CLASS_TO_THROW_EXCEPTION_ON_DESERIALIZATION
            ]
        ];
    }

    private function getUserBudgetDemandCreateRequestModel()
    {
        return (new UserBudgetDemandCreateRequestModel())
                        ->setDescription(self::DEFAULT_DESCRIPTION)
                        ->setEmail(self::DEFAULT_EMAIL)
                        ->setTelephone(self::DEFAULT_TELEPHONE)
                        ->setAddress(self::DEFAULT_ADDRESS)
                        ->setTitle(self::DEFAULT_TITLE)
                        ->setCategory(self::DEFAULT_CATEGORY);
    }

    private function getBudgetDemandUpdateRequestModel()
    {
        return (new BudgetDemandUpdateRequestModel())
                        ->setId(self::DEFAULT_ID)
                        ->setDescription(self::DEFAULT_DESCRIPTION)
                        ->setTitle(self::DEFAULT_TITLE)
                        ->setCategory(self::DEFAULT_CATEGORY);
    }

    private function getBudgetDemandListRequestModelEmailSet()
    {
        return (new BudgetDemandListRequestModel())
                        ->setEmail(self::DEFAULT_EMAIL);
    }

    private function getBudgetDemandListRequestModelEmailNotSet()
    {
        return new BudgetDemandListRequestModel();
    }

}
