<?php

declare(strict_types=1);

namespace App\Tests\Providers;

use \App\Model\UserResponseModel;
use \App\Model\BudgetDemandResponseModel;

class ResponseDataProvider
{

    public function validUserResponseModel()
    {
        return [
            [
                new UserResponseModel(), 
                '{"id": 1, "success": true, "error": null}'
            ],
            [
                new UserResponseModel(), 
                '{"id": null, "success": false, "error": "Error"}'
            ]
        ];
    }

    public function validBudgetDemandResponseModel()
    {
        return [
            [
                new BudgetDemandResponseModel(), 
                '{"id": 1, "success": true, "error": null}'
            ],
            [
                new BudgetDemandResponseModel(), 
                '{"id": 1, "success": false, "error": "Error"}'
            ]
        ];
    }

    public function invalidObj()
    {
        return [
            [null],
            [""],
            [[]],
            [new \StdClass()]
        ];
    }

}
