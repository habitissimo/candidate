<?php

declare(strict_types=1);

namespace App\Tests\Providers;

Trait IntegerDataProvider
{
    public function validIntegers()
    {
        return [
            [0],
            [1],
            [-12],
        ];
    }
}
