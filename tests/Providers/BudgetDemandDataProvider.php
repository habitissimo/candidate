<?php

declare(strict_types=1);

namespace App\Tests\Providers;

use \App\Model\BudgetDemandCreateRequestModel;
use \App\Model\BudgetDemandUpdateRequestModel;
use \App\Model\BudgetDemandListRequestModel;
use \App\Model\BudgetDemandResponseModel;
use \App\Entity\BudgetDemand;

class BudgetDemandDataProvider
{

    private const ID_DEFAULT = 4463;
    private const DESCRIPTION_DEFAULT = "A sort Description";
    private const TITLE_DEFAULT = "Whatever title";
    private const CATEGORY_DEFAULT = "Whatever category";
    private const EMAIL_DEFAULT = "whatever@email.com";
    
    public function validBudgetDemandResponseModel()
    {
        return [
            [new BudgetDemandResponseModel()]
        ];
    }

    public function validBudgetDemandCreateRequestModel()
    {
        return [
            [
                (new BudgetDemandCreateRequestModel())
                        ->setDescription(self::DESCRIPTION_DEFAULT)
                        ->setTitle(self::TITLE_DEFAULT)
                        ->setCategory(self::CATEGORY_DEFAULT)
                        ->setEmail(self::EMAIL_DEFAULT),
                '{"description": "'.self::DESCRIPTION_DEFAULT.'", "title": "'.self::TITLE_DEFAULT.'", "category": "'.self::CATEGORY_DEFAULT.'", "email": "'.self::EMAIL_DEFAULT.'" }'
            ],
            [
                (new BudgetDemandCreateRequestModel())
                        ->setDescription(self::DESCRIPTION_DEFAULT)
                        ->setTitle(self::TITLE_DEFAULT)
                        ->setEmail(self::EMAIL_DEFAULT),
                '{"description": "'.self::DESCRIPTION_DEFAULT.'", "title": "'.self::TITLE_DEFAULT.'", "email": "'.self::EMAIL_DEFAULT.'" }'
            ],
            [
                (new BudgetDemandCreateRequestModel())
                        ->setDescription(self::DESCRIPTION_DEFAULT)
                        ->setCategory(self::CATEGORY_DEFAULT)
                        ->setEmail(self::EMAIL_DEFAULT),
                '{"description": "'.self::DESCRIPTION_DEFAULT.'", "category": "'.self::CATEGORY_DEFAULT.'", "email": "'.self::EMAIL_DEFAULT.'" }'
            ],
            [
                (new BudgetDemandCreateRequestModel())
                        ->setDescription(self::DESCRIPTION_DEFAULT)
                        ->setEmail(self::EMAIL_DEFAULT),
                '{"description": "'.self::DESCRIPTION_DEFAULT.'", "email": "'.self::EMAIL_DEFAULT.'" }'
            ]
        ];
    }

    public function validBudgetDemandUpdateRequestModel()
    {
        return [
            [
                (new BudgetDemandUpdateRequestModel())
                        ->setId(self::ID_DEFAULT)
                        ->setDescription(self::DESCRIPTION_DEFAULT)
                        ->setTitle(self::TITLE_DEFAULT)
                        ->setCategory(self::CATEGORY_DEFAULT), 
                '{"id": '.self::ID_DEFAULT.', "description": "'.self::DESCRIPTION_DEFAULT.'", "title": "'.self::TITLE_DEFAULT.'", "category": "'.self::CATEGORY_DEFAULT.'"}'
            ],
            [
                (new BudgetDemandUpdateRequestModel())
                        ->setId(self::ID_DEFAULT)
                        ->setTitle(self::TITLE_DEFAULT), 
                '{"id": '.self::ID_DEFAULT.', "title": "'.self::TITLE_DEFAULT.'"}'
            ],
            [
                (new BudgetDemandUpdateRequestModel())
                        ->setId(self::ID_DEFAULT)
                        ->setDescription(self::DESCRIPTION_DEFAULT)
                        ->setCategory(self::CATEGORY_DEFAULT), 
                '{"id": '.self::ID_DEFAULT.', "description": "'.self::DESCRIPTION_DEFAULT.'", "category": "'.self::CATEGORY_DEFAULT.'"}'
            ],
            [
                (new BudgetDemandUpdateRequestModel())
                        ->setId(self::ID_DEFAULT)
                        ->setCategory(self::CATEGORY_DEFAULT), 
                '{"id": '.self::ID_DEFAULT.', "category": "'.self::CATEGORY_DEFAULT.'"}'
            ],
            [
                (new BudgetDemandUpdateRequestModel())
                        ->setId(self::ID_DEFAULT), 
                '{"id": '.self::ID_DEFAULT.'}'
            ]
        ];
    }

    public function validBudgetDemandListRequestModel()
    {
        return [
            [
                (new BudgetDemandListRequestModel())
                        ->setEmail(self::EMAIL_DEFAULT),
                1,
                '{"email": "'.self::EMAIL_DEFAULT.'"}'
            ]
        ];
    }

    public function invalidBudgetDemandListRequestModel()
    {
        return [
            [
                null,
                1,
                ''
            ]
        ];
    }

    public function validBudgetDemandCreateRequestModelMethods()
    {
        return [
            ['getEmail'],
            ['setEmail'],
            ['getTitle'],
            ['setTitle'],
            ['getDescription'],
            ['setDescription'],
            ['getCategory'],
            ['setCategory']
        ];
    }
    
    public function validBudgetDemandEntityMethods()
    {
        return [
            ['getId'],
            ['getTitle'],
            ['setTitle'],
            ['getDescription'],
            ['setDescription'],
            ['getCategory'],
            ['setCategory'],
            ['getStatus'],
            ['setStatus'],
            ['getEmail'],
            ['setEmail']
        ];
    }

    public function validBudgetDemandListRequestModelMethods()
    {
        return [
            ['getEmail'],
            ['setEmail']
        ];
    }

    public function validBudgetDemandResponseModelMethods()
    {
        return [
            ['getId'],
            ['setId'],
            ['getSuccess'],
            ['setSuccess'],
            ['getError'],
            ['setError']
        ];
    }

    public function validBudgetDemandUpdateRequestModelMethods()
    {
        return [
            ['getId'],
            ['setId'],
            ['getTitle'],
            ['setTitle'],
            ['getDescription'],
            ['setDescription'],
            ['getCategory'],
            ['setCategory']
        ];
    }
    
}
