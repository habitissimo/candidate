<?php

declare(strict_types=1);

namespace App\Tests\Providers;

use \App\Model\UserResponseModel;
use \App\Model\UserRequestModel;

class UserDataProvider
{

    private const TELEPHONE_DEFAULT = "0034 956 32 54 85";
    private const ADDRESS_DEFAULT = "Whatever St. N24";
    private const EMAIL_DEFAULT = "an_email@server.com";

    public function validUserRequestModel()
    {
        return [
            [
                (new UserRequestModel())
                        ->setTelephone(self::TELEPHONE_DEFAULT)
                        ->setAddress(self::ADDRESS_DEFAULT)
                        ->setEmail(self::EMAIL_DEFAULT), 
                '{"telephone": "0034 956 32 54 85", "address": "Whatever St. N24", "email": "an_email@server.com"}'
            ]
        ];
    }

    public function validUserResponseModel()
    {
        return [
            [new UserResponseModel()]
        ];
    }

    public function validUserBudgetDemandCreateRequestModelMethods()
    {
        return [
            ['getTelephone'],
            ['setTelephone'],
            ['getAddress'],
            ['setAddress'],
            ['getEmail'],
            ['setEmail'],
            ['getTitle'],
            ['setTitle'],
            ['getDescription'],
            ['setDescription'],
            ['getCategory'],
            ['setCategory']
        ];
    }

    public function validUserBudgetDemandResponseModelMethods()
    {
        return [
            ['getSuccess'],
            ['setSuccess'],
            ['getUser'],
            ['setUser'],
            ['getBudgetDemand'],
            ['setBudgetDemand']
        ];
    }

    public function validUserRequestModelMethods()
    {
        return [
            ['getTelephone'],
            ['setTelephone'],
            ['getAddress'],
            ['setAddress'],
            ['getEmail'],
            ['setEmail']
        ];
    }

    public function validUserResponseModelMethods()
    {
        return [
            ['getId'],
            ['setId'],
            ['getSuccess'],
            ['setSuccess'],
            ['getError'],
            ['setError']
        ];
    }

    public function validBudgetDemandEntityMethods()
    {
        return [
            ['getId'],
            ['getEmail'],
            ['setEmail'],
            ['getTelephone'],
            ['setTelephone'],
            ['getAddress'],
            ['setAddress']
        ];
    }

}
