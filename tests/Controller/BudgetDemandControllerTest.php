<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use \App\Service\RequestService;
use \App\Service\UserBudgetDemandService;
use \App\Service\BudgetDemandService;
use \App\Service\ResponseService;
use \App\Model\UserBudgetDemandCreateRequestModel;
use \App\Model\BudgetDemandUpdateRequestModel;
use \App\Model\BudgetDemandListRequestModel;
use \App\Model\UserBudgetDemandResponseModel;
use \App\Model\BudgetDemandResponseModel;

use \App\Controller\BudgetDemandController;

class BudgetDemandControllerTest extends TestCase
{

    private $requestServiceMock;
    private $userBudgetDemandServiceMock;
    private $budgetDemandServiceMock;
    private $responseServiceMock;

    private $userBudgetDemandResponseModelMock;
    private $budgetDemandResponseModelMock;
    private $requestMock;
    private $responseMock;

    private $budgetDemandController;

    /** @before */
    public function setUp()
    {
        $this->requestServiceMock = $this->createMock(RequestService::class);
        $this->userBudgetDemandServiceMock = $this->createMock(UserBudgetDemandService::class);
        $this->budgetDemandServiceMock = $this->createMock(BudgetDemandService::class);
        $this->responseServiceMock = $this->createMock(ResponseService::class);

        $this->requestMock = $this->createMock(Request::class);

        $this->userBudgetDemandResponseModelMock = $this->createMock(UserBudgetDemandResponseModel::class);
        $this->budgetDemandResponseModelMock = $this->createMock(BudgetDemandResponseModel::class);
        
        $this->responseMock = $this->createMock(Response::class);

        $this->budgetDemandController = new BudgetDemandController(
            $this->requestServiceMock,
            $this->userBudgetDemandServiceMock,
            $this->budgetDemandServiceMock,
            $this->responseServiceMock
        );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserBudgetDemandDataProvider::validUserBudgetDemandCreateRequestModel
     */
    public function budgetDemandCreate(
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel
    )
    {
        $this->givenGetUserBudgetDemandCreateStepIsConfigured($userBudgetDemandCreateRequestModel);
        $this->andUserCreateOrUpdateAndBudgetDemandCreateStepIsConfigured($userBudgetDemandCreateRequestModel);
        $this->andGetResponseStepIsConfigured($this->userBudgetDemandResponseModelMock);
        $reponse = $this->whenBudgetDemandCreateIsCalled();
        $this->thenBudgetDemandActionHasBeenDone($reponse);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function budgetDemandUpdate(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel
    )
    {
        $this->givenGetBudgetDemandUpdateStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andUpdateStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andGetResponseStepIsConfigured($this->budgetDemandResponseModelMock);
        $reponse = $this->whenBudgetDemandUpdateIsCalled();
        $this->thenBudgetDemandActionHasBeenDone($reponse);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function budgetDemandPublish(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel
    )
    {
        $this->givenGetBudgetDemandUpdateStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andPublishStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andGetResponseStepIsConfigured($this->budgetDemandResponseModelMock);
        $reponse = $this->whenBudgetDemandPublishIsCalled();
        $this->thenBudgetDemandActionHasBeenDone($reponse);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function budgetDemandDiscard(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel
    )
    {
        $this->givenGetBudgetDemandUpdateStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andDiscardStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andGetResponseStepIsConfigured($this->budgetDemandResponseModelMock);
        $reponse = $this->whenBudgetDemandDiscardIsCalled();
        $this->thenBudgetDemandActionHasBeenDone($reponse);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandListRequestModel
     */
    public function budgetDemandList(
        BudgetDemandListRequestModel $budgetDemandListRequestModel,
        int $page,
        string $budgetDemandListRequestModelSerializedHelper
    )
    {
        $this->givenGetBudgetDemandListStepIsConfigured($budgetDemandListRequestModel);
        $arrayIterator = $this->andListStepIsConfigured($budgetDemandListRequestModel, $page);
        $this->andGetResponseStepIsConfigured($arrayIterator);
        $reponse = $this->whenBudgetDemandListIsCalled();
        $this->thenBudgetDemandActionHasBeenDone($reponse);
    }

    private function givenGetUserBudgetDemandCreateStepIsConfigured(
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel
    )
    {
        $this->requestServiceMock
                ->expects($this->once())
                ->method('getUserBudgetDemandCreate')
                ->with($this->requestMock)
                ->willReturn($userBudgetDemandCreateRequestModel);
    }

    private function givenGetBudgetDemandUpdateStepIsConfigured(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel
    )
    {
        $this->requestServiceMock
                ->expects($this->once())
                ->method('getBudgetDemandUpdate')
                ->with($this->requestMock)
                ->willReturn($budgetDemandUpdateRequestModel);
    }

    private function givenGetBudgetDemandListStepIsConfigured(
        BudgetDemandListRequestModel $budgetDemandListRequestModel
    )
    {
        $this->requestServiceMock
                ->expects($this->once())
                ->method('getBudgetDemandList')
                ->with($this->requestMock)
                ->willReturn($budgetDemandListRequestModel);
    }

    private function andUserCreateOrUpdateAndBudgetDemandCreateStepIsConfigured(
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel
    )
    {
        $this->userBudgetDemandServiceMock
                ->expects($this->once())
                ->method('userCreateOrUpdateAndBudgetDemandCreate')
                ->with($userBudgetDemandCreateRequestModel)
                ->willReturn($this->userBudgetDemandResponseModelMock);
    }

    private function andUpdateStepIsConfigured(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel
    )
    {
        $this->budgetDemandServiceMock
                ->expects($this->once())
                ->method('update')
                ->with($budgetDemandUpdateRequestModel)
                ->willReturn($this->budgetDemandResponseModelMock);
    }

    private function andPublishStepIsConfigured(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel
    )
    {
        $this->budgetDemandServiceMock
                ->expects($this->once())
                ->method('publish')
                ->with($budgetDemandUpdateRequestModel)
                ->willReturn($this->budgetDemandResponseModelMock);
    }

    private function andDiscardStepIsConfigured(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel
    )
    {
        $this->budgetDemandServiceMock
                ->expects($this->once())
                ->method('discard')
                ->with($budgetDemandUpdateRequestModel)
                ->willReturn($this->budgetDemandResponseModelMock);
    }

    private function andListStepIsConfigured(
        BudgetDemandListRequestModel $budgetDemandListRequestModel,
        $page = 1
    )
    {
        $arrayIterator = new \ArrayIterator();

        $this->requestMock
                ->expects($this->once())
                ->method('get')
                ->with('page')
                ->willReturn($page);
        
        $this->budgetDemandServiceMock
                ->expects($this->once())
                ->method('list')
                ->with($budgetDemandListRequestModel, $page)
                ->willReturn($arrayIterator);

        return $arrayIterator;
    }

    private function andGetResponseStepIsConfigured($param)
    {
        $this->responseServiceMock
                ->expects($this->once())
                ->method('getResponse')
                ->with($param)
                ->willReturn($this->responseMock);
    }

    private function whenBudgetDemandCreateIsCalled()
    {
        return $this->budgetDemandController->budgetDemandCreate($this->requestMock);
    }

    private function whenBudgetDemandUpdateIsCalled()
    {
        return $this->budgetDemandController->budgetDemandUpdate($this->requestMock);
    }

    private function whenBudgetDemandPublishIsCalled()
    {
        return $this->budgetDemandController->budgetDemandPublish($this->requestMock);
    }

    private function whenBudgetDemandDiscardIsCalled()
    {
        return $this->budgetDemandController->budgetDemandDiscard($this->requestMock);
    }

    private function whenBudgetDemandListIsCalled()
    {
        return $this->budgetDemandController->budgetDemandList($this->requestMock);
    }

    private function thenBudgetDemandActionHasBeenDone(Response $reponse)
    {
        $this->assertThat($this->responseMock, $this->equalTo($reponse));
    }

}
