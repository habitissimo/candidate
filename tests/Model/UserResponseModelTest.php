<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\UserResponseModel;

class UserResponseModelTest extends TestCase
{

    private $reflectionClass;
    
    /** before */
    public function setUp()
    {
        $this->reflectionClass = new \ReflectionClass(UserResponseModel::class);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserDataProvider::validUserResponseModelMethods
    */
    public function methodsShouldBePresent(string $functionName)
    {
        $this->assertThat(true, $this->equalTo($this->reflectionClass->hasMethod($functionName)));
    }

    /**
     * @test
    */
    public function numberOfMethodsAllowedIs6()
    {
        $this->assertThat(6, $this->equalTo(count($this->reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC))));
    }

}
