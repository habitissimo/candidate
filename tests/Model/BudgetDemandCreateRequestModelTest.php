<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\BudgetDemandCreateRequestModel;

class BudgetDemandCreateRequestModelTest extends TestCase
{

    private $reflectionClass;
    
    /** before */
    public function setUp()
    {
        $this->reflectionClass = new \ReflectionClass(BudgetDemandCreateRequestModel::class);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandCreateRequestModelMethods
    */
    public function methodsShouldBePresent(string $functionName)
    {
        $this->assertThat(true, $this->equalTo($this->reflectionClass->hasMethod($functionName)));
    }

    /**
     * @test
    */
    public function numberOfMethodsAllowedIs8()
    {
        $this->assertThat(8, $this->equalTo(count($this->reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC))));
    }

}
