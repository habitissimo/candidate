<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\UserBudgetDemandResponseModel;
use \App\Model\UserResponseModel;
use \App\Model\BudgetDemandResponseModel;

class UserBudgetDemandResponseModelTest extends TestCase
{

    private $reflectionClass;
    private $userBudgetDemandResponseModelMock;

    /** before */
    public function setUp()
    {
        $this->userBudgetDemandResponseModelMock = $this->createMock(UserBudgetDemandResponseModel::class);
        $this->reflectionClass = new \ReflectionClass(UserBudgetDemandResponseModel::class);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserDataProvider::validUserBudgetDemandResponseModelMethods
    */
    public function methodsShouldBePresent(string $functionName)
    {
        $this->assertThat(true, $this->equalTo($this->reflectionClass->hasMethod($functionName)));
    }

    /**
     * @test
    */
    public function numberOfMethodsAllowedIs6()
    {
        $this->assertThat(6, $this->equalTo(count($this->reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC))));
    }

    /** @test */
    public function getUserShouldReturnNullBeforeSetUser()
    {
        $this->assertThat(null, $this->equalTo($this->userBudgetDemandResponseModelMock->getUser()));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserDataProvider::validUserResponseModel
    */
    public function setUserShouldReturnUserBudgetDemandResponseModelClass(UserResponseModel $user)
    {
        $this->assertInstanceOf(UserBudgetDemandResponseModel::class, $this->userBudgetDemandResponseModelMock->setUser($user));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserDataProvider::validUserResponseModel
    */
    public function getUserShouldReturnUserResponseModelClassAfterSetUser(UserResponseModel $user)
    {
        $userBudgetDemandResponseModel = (new UserBudgetDemandResponseModel())->setUser($user);

        $this->userBudgetDemandResponseModelMock
                ->method('setUser')
                ->willReturn($userBudgetDemandResponseModel);
        
        $this->assertInstanceOf(UserResponseModel::class, $this->userBudgetDemandResponseModelMock->setUser($user)->getUser());
    }

    /** @test */
    public function getBudgetDemandShouldReturnNullBeforeSetBudgetDemand()
    {
        $this->assertThat(null, $this->equalTo($this->userBudgetDemandResponseModelMock->getBudgetDemand()));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandResponseModel
    */
    public function setBudgetDemandShouldReturnUserBudgetDemandResponseModelClass(BudgetDemandResponseModel $budgetDemand)
    {
        $this->assertInstanceOf(
            UserBudgetDemandResponseModel::class, 
            $this->userBudgetDemandResponseModelMock->setBudgetDemand($budgetDemand)
        );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandResponseModel
    */
    public function getBudgetDemandShouldReturnUserResponseModelClassAfterSetBudgetDemand(BudgetDemandResponseModel $budgetDemand)
    {
        $userBudgetDemandResponseModel = (new UserBudgetDemandResponseModel())->setBudgetDemand($budgetDemand);

        $this->userBudgetDemandResponseModelMock
                ->method('setBudgetDemand')
                ->willReturn($userBudgetDemandResponseModel);
        
        $this->assertInstanceOf(
            BudgetDemandResponseModel::class, 
            $this->userBudgetDemandResponseModelMock
                    ->setBudgetDemand($budgetDemand)
                    ->getBudgetDemand()
        );
    }

}
