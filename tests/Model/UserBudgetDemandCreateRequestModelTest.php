<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\UserBudgetDemandCreateRequestModel;

class UserBudgetDemandCreateRequestModelTest extends TestCase
{

    private $reflectionClass;

    /** before */
    public function setUp()
    {
        $this->reflectionClass = new \ReflectionClass(UserBudgetDemandCreateRequestModel::class);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserDataProvider::validUserBudgetDemandCreateRequestModelMethods
    */
    public function methodsShouldBePresent(string $functionName)
    {
        $this->assertThat(true, $this->equalTo($this->reflectionClass->hasMethod($functionName)));
    }

    /**
     * @test
    */
    public function numberOfMethodsAllowedIs12()
    {
        $this->assertThat(12, $this->equalTo(count($this->reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC))));
    }

}
