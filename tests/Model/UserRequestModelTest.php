<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\UserRequestModel;

class UserRequestModelTest extends TestCase
{

    private $reflectionClass;

    /** before */
    public function setUp()
    {
        $this->reflectionClass = new \ReflectionClass(UserRequestModel::class);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserDataProvider::validUserRequestModelMethods
    */
    public function methodsShouldBePresent(string $functionName)
    {
        $this->assertThat(true, $this->equalTo($this->reflectionClass->hasMethod($functionName)));
    }

    /**
     * @test
    */
    public function numberOfMethodsAllowedIs6()
    {
        $this->assertThat(6, $this->equalTo(count($this->reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC))));
    }

}
