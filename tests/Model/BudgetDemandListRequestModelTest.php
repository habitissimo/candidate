<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\BudgetDemandListRequestModel;

class BudgetDemandListRequestModelTest extends TestCase
{

    private $reflectionClass;
    
    /** before */
    public function setUp()
    {
        $this->reflectionClass = new \ReflectionClass(BudgetDemandListRequestModel::class);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandListRequestModelMethods
    */
    public function methodsShouldBePresent(string $functionName)
    {
        $this->assertThat(true, $this->equalTo($this->reflectionClass->hasMethod($functionName)));
    }

    /**
     * @test
    */
    public function numberOfMethodsAllowedIs2()
    {
        $this->assertThat(2, $this->equalTo(count($this->reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC))));
    }

}
