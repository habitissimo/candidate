<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\BudgetDemandResponseModel;

class BudgetDemandResponseModelTest extends TestCase
{

    private $reflectionClass;
    
    /** before */
    public function setUp()
    {
        $this->reflectionClass = new \ReflectionClass(BudgetDemandResponseModel::class);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandResponseModelMethods
    */
    public function methodsShouldBePresent(string $functionName)
    {
        $this->assertThat(true, $this->equalTo($this->reflectionClass->hasMethod($functionName)));
    }

    /**
     * @test
    */
    public function numberOfMethodsAllowedIs6()
    {
        $this->assertThat(6, $this->equalTo(count($this->reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC))));
    }

}
