<?php

declare(strict_types=1);

namespace App\Tests\Model\Feature\Optional;

use PHPUnit\Framework\TestCase;

class CategoryTraitTest extends TestCase
{
    use \App\Model\Feature\Optional\CategoryTrait;

    /** @test */
    public function getCategoryShouldBeNullBeforeSetCategory()
    {
        $this->assertThat(null, $this->equalTo($this->getCategory()));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\Optional\CategoryDataProvider::validStrings
     */
    public function setCategoryShouldReturnTheClassUsingTheTraitInstance($category)
    {
        $this->assertInstanceOf(get_class($this), $this->setCategory($category));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\Optional\CategoryDataProvider::validStrings
     */
    public function getCategoryShouldReturnTheSameValueAfterSetCategoryIsPerformed($category)
    {
        $this->setCategory($category);
        $this->assertThat($category, $this->equalTo($this->getCategory()));
    }

}
