<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class ErrorTraitTest extends TestCase
{
    use \App\Model\Feature\ErrorTrait;

    /** @test */
    public function getErrorShouldBeNullBeforeSetError()
    {
        $this->assertThat(null, $this->equalTo($this->getError()));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\ErrorDataProvider::validStrings
    */
    public function setErrorShouldReturnTheClassUsingTheTraitInstance($error)
    {
        $this->assertInstanceOf(get_class($this), $this->setError($error));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\ErrorDataProvider::validStrings
    */
    public function getErrorShouldReturnTheSameValueAfterSetErrorIsPerformed($error)
    {
        $this->setError($error);
        $this->assertThat($error, $this->equalTo($this->getError()));
    }

}
