<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class AddressTraitTest extends TestCase
{
    use \App\Model\Feature\Required\AddressTrait;

    /** @test */
    public function getAddressShouldBeNullBeforeSetAddress()
    {
        $this->assertThat(null, $this->equalTo($this->getAddress()));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\Required\AddressDataProvider::validStrings
    */
    public function setAddressShouldReturnTheClassUsingTheTraitInstance($address)
    {
        $this->assertInstanceOf(get_class($this), $this->setAddress($address));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\Required\AddressDataProvider::validStrings
    */
    public function getAddressShouldReturnTheSameValueAfterSetAddressIsPerformed($address)
    {
        $this->setAddress($address);
        $this->assertThat($address, $this->equalTo($this->getAddress()));
    }

}
