<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class EmailTraitTest extends TestCase
{
    use \App\Model\Feature\Required\EmailTrait;

    /** @test */
    public function getEmailShouldBeNullBeforeSetEmail()
    {
        $this->assertThat(null, $this->equalTo($this->getEmail()));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\Required\EmailDataProvider::validStrings
    */
    public function setEmailShouldReturnTheClassUsingTheTraitInstance($email)
    {
        $this->assertInstanceOf(get_class($this), $this->setEmail($email));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\Required\EmailDataProvider::validStrings
    */
    public function getEmailShouldReturnTheSameValueAfterSetEmailIsPerformed($email)
    {
        $this->setEmail($email);
        $this->assertThat($email, $this->equalTo($this->getEmail()));
    }

}
