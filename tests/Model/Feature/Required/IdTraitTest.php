<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class IdTraitTest extends TestCase
{
    use \App\Model\Feature\Required\IdTrait;

    /** @test */
    public function getIdShouldBeNullBeforeSetId()
    {
        $this->assertThat(null, $this->equalTo($this->getId()));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\Required\IdDataProvider::validIntegers
    */
    public function setIdShouldReturnTheClassUsingTheTraitInstance($id)
    {
        $this->assertInstanceOf(get_class($this), $this->setId($id));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\Required\IdDataProvider::validIntegers
    */
    public function getIdShouldReturnTheSameValueAfterSetIdIsPerformed($id)
    {
        $this->setId($id);
        $this->assertThat($id, $this->equalTo($this->getId()));
    }

}
