<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class TelephoneTraitTest extends TestCase
{
    use \App\Model\Feature\Required\TelephoneTrait;

    /** @test */
    public function getTelephoneShouldBeNullBeforeSetTelephone()
    {
        $this->assertThat(null, $this->equalTo($this->getTelephone()));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\Required\TelephoneDataProvider::validStrings
    */
    public function setTelephoneShouldReturnTheClassUsingTheTraitInstance($telephone)
    {
        $this->assertInstanceOf(get_class($this), $this->setTelephone($telephone));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\Required\TelephoneDataProvider::validStrings
    */
    public function getTelephoneShouldReturnTheSameValueAfterSetTelephoneIsPerformed($telephone)
    {
        $this->setTelephone($telephone);
        $this->assertThat($telephone, $this->equalTo($this->getTelephone()));
    }

}
