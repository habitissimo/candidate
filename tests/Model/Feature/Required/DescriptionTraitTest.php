<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class DescriptionTraitTest extends TestCase
{
    use \App\Model\Feature\Required\DescriptionTrait;

    /** @test */
    public function getDescriptionShouldBeNullBeforeSetDescription()
    {
        $this->assertThat(null, $this->equalTo($this->getDescription()));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\Required\DescriptionDataProvider::validStrings
    */
    public function setDescriptionShouldReturnTheClassUsingTheTraitInstance($description)
    {
        $this->assertInstanceOf(get_class($this), $this->setDescription($description));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\Required\DescriptionDataProvider::validStrings
    */
    public function getDescriptionShouldReturnTheSameValueAfterSetDescriptionIsPerformed($description)
    {
        $this->setDescription($description);
        $this->assertThat($description, $this->equalTo($this->getDescription()));
    }

}
