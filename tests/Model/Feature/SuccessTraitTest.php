<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class SuccessTraitTest extends TestCase
{
    use \App\Model\Feature\SuccessTrait;

    /** @test */
    public function getSuccessShouldBeNullBeforeSetSuccess()
    {
        $this->assertThat(null, $this->equalTo($this->getSuccess()));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\SuccessDataProvider::validStrings
    */
    public function setSuccessShouldReturnTheClassUsingTheTraitInstance($success)
    {
        $this->assertInstanceOf(get_class($this), $this->setSuccess($success));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\Feature\SuccessDataProvider::validStrings
    */
    public function getSuccessShouldReturnTheSameValueAfterSetSuccessIsPerformed($success)
    {
        $this->setSuccess($success);
        $this->assertThat(!!$success, $this->equalTo($this->getSuccess()));
    }

}
