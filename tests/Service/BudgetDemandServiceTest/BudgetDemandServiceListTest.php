<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Serializer\SerializerInterface;

use Doctrine\ORM\EntityManagerInterface;

use \App\Service\ValidateService;

use \App\Model\BudgetDemandListRequestModel;
use \App\Entity\BudgetDemand;
use \App\Model\BudgetDemandResponseModel;

use \App\Service\BudgetDemandService;
use \App\Repository\BudgetDemandRepository;

class BudgetDemandServiceListTest extends TestCase
{

    private const ERROR_MESSAGE_GENERAL = "Error persisting data.";
    private const DEFAULT_EMAIL = "whatever@email.com.";

    private $entityManagerInterfaceMock;
    private $validateServiceMock;
    private $serializerInterfaceMock;
    private $budgetDemandResponseModelMock;
    private $budgetDemandMock;
    private $BudgetDemandRepositoryMock;

    private $budgetDemandService;

    /** before */
    public function setUp()
    {
        $this->entityManagerInterfaceMock = $this->createMock(EntityManagerInterface::class);
        $this->validateServiceMock = $this->createMock(ValidateService::class);
        $this->serializerInterfaceMock = $this->createMock(SerializerInterface::class);
        $this->budgetDemandResponseModelMock = $this->createMock(BudgetDemandResponseModel::class);
        $this->budgetDemandMock = $this->createMock(BudgetDemand::class);
        $this->BudgetDemandRepositoryMock = $this->createMock(BudgetDemandRepository::class);

        $this->budgetDemandService = new BudgetDemandService(
            $this->entityManagerInterfaceMock,
            $this->validateServiceMock,
            $this->serializerInterfaceMock,
            $this->budgetDemandResponseModelMock
        );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandListRequestModel
     */
    public function listShouldReturnBudgetDemandListPaginatedByEmail(
        BudgetDemandListRequestModel $budgetDemandListRequestModel,
        $page = 1,
        $data
    )
    {
        $this->givenValidateMethodStepIsConfigured($budgetDemandListRequestModel);
        $paginator = $this->andEmailIsFoundIntoDBStepIsConfigured($budgetDemandListRequestModel, $page);
        $this->andPaginatorIteratorStepIsConfigured($paginator, $this->budgetDemandMock, BudgetDemandRepository::LIMIT);
        $this->andSetSuccessStepIsConfigured($budgetDemandListRequestModel);
        $budgetDemandResponseModelList = $this->whenListIsCalled($budgetDemandListRequestModel);
        $this->thenTheBudgetDemandEntityDBHasBeenListed($budgetDemandListRequestModel, $budgetDemandResponseModelList);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::invalidBudgetDemandListRequestModel
     */
    public function listShouldReturnAllBudgetDemandListPaginated(
        ?BudgetDemandListRequestModel $budgetDemandListRequestModel,
        $page = 1,
        $data
    )
    {
        $paginator = $this->givenListPaginatedAllStepIsConfigured($budgetDemandListRequestModel, $page);
        $this->andPaginatorIteratorStepIsConfigured($paginator, $this->budgetDemandMock, BudgetDemandRepository::LIMIT);
        $this->andSetSuccessStepIsConfigured($budgetDemandListRequestModel);
        $budgetDemandResponseModelList = $this->whenListIsCalled($budgetDemandListRequestModel);
        $this->thenTheBudgetDemandEntityDBHasBeenListed($budgetDemandListRequestModel, $budgetDemandResponseModelList);
    }

    private function givenValidateMethodStepIsConfigured(BudgetDemandListRequestModel $budgetDemandListRequestModel)
    {
        $this->validateServiceMock
                ->expects($this->once())
                ->method('validate')
                ->with($budgetDemandListRequestModel)
                ->willReturn($budgetDemandListRequestModel);
    }

    private function andEmailIsFoundIntoDBStepIsConfigured(BudgetDemandListRequestModel $budgetDemandListRequestModel, $page)
    {
        $paginator = $this->createMock(Doctrine\ORM\Tools\Pagination\Paginator::class);
        $this->entityManagerInterfaceMock
                ->expects($this->once())
                ->method('getRepository')
                ->with('\App\Entity\BudgetDemand')
                ->willReturn($this->BudgetDemandRepositoryMock);

        $this->BudgetDemandRepositoryMock
                ->expects($this->once())
                ->method('listPaginatedByEmail')
                ->with($budgetDemandListRequestModel->getEmail(), $page)
                ->willReturn($paginator);

        return $paginator;
    }

    private function givenListPaginatedAllStepIsConfigured(?BudgetDemandListRequestModel $budgetDemandListRequestModel, $page)
    {
        $paginator = $this->createMock(Doctrine\ORM\Tools\Pagination\Paginator::class);
        $this->entityManagerInterfaceMock
                ->expects($this->once())
                ->method('getRepository')
                ->with('\App\Entity\BudgetDemand')
                ->willReturn($this->BudgetDemandRepositoryMock);

        $this->BudgetDemandRepositoryMock
                ->expects($this->once())
                ->method('listPaginatedAll')
                ->with($page)
                ->willReturn($paginator);

        return $paginator;
    }

    private function andPaginatorIteratorStepIsConfigured($paginator, $itemToAppend, $times = BudgetDemandRepository::LIMIT)
    {
        $arrayIterator = new \ArrayIterator();
        for($i = 0; $i < $times; $i++) {
            $arrayIterator->append($itemToAppend);
        }

        $paginator
            ->expects($this->once())
            ->method('getIterator')
            ->willReturn($arrayIterator); 
    }

    private function andSetSuccessStepIsConfigured(?BudgetDemandListRequestModel $budgetDemandListRequestModel)
    {
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getId')
                ->willReturn(1);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getStatus')
                ->willReturn(BudgetDemandService::DISCARDED_STATUS);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getEmail')
                ->willReturn(self::DEFAULT_EMAIL);
    }

    private function whenListIsCalled(?BudgetDemandListRequestModel $budgetDemandListRequestModel): \ArrayIterator
    {
        return $this->budgetDemandService->list($budgetDemandListRequestModel);
    }

    private function thenTheBudgetDemandEntityDBHasBeenListed(
        ?BudgetDemandListRequestModel $budgetDemandListRequestModel,
        \ArrayIterator $budgetDemandResponseModelList
    )
    {
        $this->assertThat(count($budgetDemandResponseModelList), $this->equalTo(BudgetDemandRepository::LIMIT));
    }

}
