<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Serializer\SerializerInterface;

use Doctrine\ORM\EntityManagerInterface;

use \App\Service\ValidateService;

use \App\Model\BudgetDemandCreateRequestModel;
use \App\Entity\BudgetDemand;
use \App\Model\BudgetDemandResponseModel;

use \App\Service\BudgetDemandService;
use \App\Repository\BudgetDemandRepository;

class BudgetDemandServiceCreateTest extends TestCase
{

    private const ERROR_MESSAGE_GENERAL = "Error persisting data.";

    private $entityManagerInterfaceMock;
    private $validateServiceMock;
    private $serializerInterfaceMock;
    private $budgetDemandResponseModelMock;
    private $budgetDemandMock;
    private $BudgetDemandRepositoryMock;

    private $budgetDemandService;

    /** before */
    public function setUp()
    {
        $this->entityManagerInterfaceMock = $this->createMock(EntityManagerInterface::class);
        $this->validateServiceMock = $this->createMock(ValidateService::class);
        $this->serializerInterfaceMock = $this->createMock(SerializerInterface::class);
        $this->budgetDemandResponseModelMock = $this->createMock(BudgetDemandResponseModel::class);
        $this->budgetDemandMock = $this->createMock(BudgetDemand::class);
        $this->BudgetDemandRepositoryMock = $this->createMock(BudgetDemandRepository::class);

        $this->budgetDemandService = new BudgetDemandService(
            $this->entityManagerInterfaceMock,
            $this->validateServiceMock,
            $this->serializerInterfaceMock,
            $this->budgetDemandResponseModelMock
        );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandCreateRequestModel
     */
    public function createShouldSuccess(BudgetDemandCreateRequestModel $budgetDemandCreateRequestModel, $data)
    {
        $this->givenGetEntityMethodStepIsConfigured($budgetDemandCreateRequestModel, $data);
        $this->andSetSuccessStepIsConfigured();
        $budgetDemandResponseModel = $this->whenCreateIsCalled($budgetDemandCreateRequestModel);
        $this->thenTheBudgetDemandEntityHasBeenCreated($budgetDemandCreateRequestModel, $budgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandCreateRequestModel
     */
    public function createShouldFails(BudgetDemandCreateRequestModel $budgetDemandCreateRequestModel, $data)
    {
        $this->givenGetEntityMethodStepIsConfigured($budgetDemandCreateRequestModel, $data);
        $this->andSetFailureStepIsConfigured("Error persisting data.");
        $this->andSetFailureDueToPersistingBudgetDemandStepIsConfigured();
        $budgetDemandResponseModel = $this->whenCreateIsCalled($budgetDemandCreateRequestModel);
        $this->thenTheBudgetDemandEntityHasNotBeenCreated($budgetDemandCreateRequestModel, $budgetDemandResponseModel);
    }

    private function givenGetEntityMethodStepIsConfigured(BudgetDemandCreateRequestModel $budgetDemandCreateRequestModel, string $data)
    {
        $this->budgetDemandMock
                ->method('getTitle')
                ->willReturn($budgetDemandCreateRequestModel->getTitle());
        $this->budgetDemandMock
                ->method('getDescription')
                ->willReturn($budgetDemandCreateRequestModel->getDescription());
        $this->budgetDemandMock
                ->method('getCategory')
                ->willReturn($budgetDemandCreateRequestModel->getCategory());
        $this->budgetDemandMock
                ->method('getEmail')
                ->willReturn($budgetDemandCreateRequestModel->getEmail());

        $this->serializerInterfaceMock
                ->expects($this->once())
                ->method('serialize')
                ->with(
                    $budgetDemandCreateRequestModel,
                    'json'
                )
                ->willReturn($data);

        $this->serializerInterfaceMock
                ->expects($this->once())
                ->method('deserialize')
                ->with(
                    $data,
                    BudgetDemand::class,
                    'json'
                )
                ->willReturn($this->budgetDemandMock);
    }

    private function andSetSuccessStepIsConfigured()
    {
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getId')
                ->willReturn(1);

        $this->budgetDemandResponseModelMock
                ->method('getId')
                ->willReturn($this->budgetDemandMock->getId());
        $this->budgetDemandResponseModelMock
                ->expects($this->once())
                ->method('getSuccess')
                ->willReturn(true);
        $this->budgetDemandResponseModelMock
                ->method('getError')
                ->willReturn(null);
    }

    private function andSetFailureStepIsConfigured(string $failureMessage, $status = BudgetDemandService::PENDING_STATUS)
    {
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getId')
                ->willReturn(null);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getStatus')
                ->willReturn($status);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getDescription')
                ->willReturn("");
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getTitle')
                ->willReturn("");
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getCategory')
                ->willReturn("");

        $this->budgetDemandResponseModelMock
                ->method('getId')
                ->willReturn(null);
        $this->budgetDemandResponseModelMock
                ->method('getSuccess')
                ->willReturn(false);
        $this->budgetDemandResponseModelMock
                ->expects($this->once())
                ->method('getError')
                ->willReturn($failureMessage);
    }

    private function andSetFailureDueToPersistingBudgetDemandStepIsConfigured()
    {
        $this->entityManagerInterfaceMock
                ->expects($this->once())
                ->method('persist')
                ->will($this->throwException(new \Exception));
    }

    private function whenCreateIsCalled(BudgetDemandCreateRequestModel $budgetDemandCreateRequestModel): BudgetDemandResponseModel
    {
        return $this->budgetDemandService->create($budgetDemandCreateRequestModel);
    }

    private function thenTheBudgetDemandEntityHasBeenCreated(
        BudgetDemandCreateRequestModel $budgetDemandCreateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat(true, $this->equalTo($budgetDemandResponseModel->getSuccess()));
    }

    private function thenTheBudgetDemandEntityHasNotBeenCreated(
        BudgetDemandCreateRequestModel $budgetDemandCreateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat(self::ERROR_MESSAGE_GENERAL, $this->equalTo($budgetDemandResponseModel->getError()));
    }

}
