<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Serializer\SerializerInterface;

use Doctrine\ORM\EntityManagerInterface;

use \App\Service\ValidateService;

use \App\Model\BudgetDemandUpdateRequestModel;
use \App\Entity\BudgetDemand;
use \App\Model\BudgetDemandResponseModel;

use \App\Service\BudgetDemandService;
use \App\Repository\BudgetDemandRepository;

class BudgetDemandServiceDiscardTest extends TestCase
{

    private const ERROR_MESSAGE_GENERAL = "Error persisting data.";

    private $entityManagerInterfaceMock;
    private $validateServiceMock;
    private $serializerInterfaceMock;
    private $budgetDemandResponseModelMock;
    private $budgetDemandMock;
    private $BudgetDemandRepositoryMock;

    private $budgetDemandService;

    /** before */
    public function setUp()
    {
        $this->entityManagerInterfaceMock = $this->createMock(EntityManagerInterface::class);
        $this->validateServiceMock = $this->createMock(ValidateService::class);
        $this->serializerInterfaceMock = $this->createMock(SerializerInterface::class);
        $this->budgetDemandResponseModelMock = $this->createMock(BudgetDemandResponseModel::class);
        $this->budgetDemandMock = $this->createMock(BudgetDemand::class);
        $this->BudgetDemandRepositoryMock = $this->createMock(BudgetDemandRepository::class);

        $this->budgetDemandService = new BudgetDemandService(
            $this->entityManagerInterfaceMock,
            $this->validateServiceMock,
            $this->serializerInterfaceMock,
            $this->budgetDemandResponseModelMock
        );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function dicardShouldSuccess(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel, $data)
    {
        $this->givenValidateMethodStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetSuccessStepIsConfigured($budgetDemandUpdateRequestModel);
        $budgetDemandResponseModel = $this->whenDiscardIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasBeenDiscarded($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function dicardShouldFailsDueToExceptionPersistingBudgetDemand(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        $data
    )
    {
        $this->givenValidateMethodStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetFailureStepIsConfigured(
            self::ERROR_MESSAGE_GENERAL,
            BudgetDemandService::PENDING_STATUS
        );
        $this->andSetFailureDueToPersistingBudgetDemandStepIsConfigured();
        $budgetDemandResponseModel = $this->whenDiscardIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasNotBeenDiscarded($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function discardShouldFailsDueToBudgetDemandStatusIsDiscarded(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        $data
    )
    {
        $this->givenValidateMethodStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetFailureStepIsConfigured(
            BudgetDemandService::BUDGET_DEMAND_ALREADY_DISCARDED,
            BudgetDemandService::DISCARDED_STATUS
        );

        $budgetDemandResponseModel = $this->whenDiscardIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityFailsDueToStatusIsAlreadyDiscarded($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    private function givenValidateMethodStepIsConfigured(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $this->validateServiceMock
                ->expects($this->once())
                ->method('validate')
                ->with($budgetDemandUpdateRequestModel)
                ->willReturn($budgetDemandUpdateRequestModel);
    }

    private function andIdIsFoundIntoDBStepIsConfigured(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $this->configBudgetDemandRepository($this->budgetDemandMock, $budgetDemandUpdateRequestModel);
    }

    private function givenIdIsNotFoundIntoDB(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $this->configBudgetDemandRepository(null, $budgetDemandUpdateRequestModel);
    }

    private function configBudgetDemandRepository($returnObject, BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $this->entityManagerInterfaceMock
                ->expects($this->once())
                ->method('getRepository')
                ->with('\App\Entity\BudgetDemand')
                ->willReturn($this->BudgetDemandRepositoryMock);

        $this->BudgetDemandRepositoryMock
                ->expects($this->once())
                ->method('find')
                ->with($budgetDemandUpdateRequestModel->getId())
                ->willReturn($returnObject);
    }

    private function andSetSuccessStepIsConfigured(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getId')
                ->willReturn(1);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getStatus')
                ->willReturn(BudgetDemandService::DISCARDED_STATUS);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getDescription')
                ->willReturn($budgetDemandUpdateRequestModel->getDescription());
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getTitle')
                ->willReturn($budgetDemandUpdateRequestModel->getTitle());
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getCategory')
                ->willReturn($budgetDemandUpdateRequestModel->getCategory());
    }

    private function andSetFailureStepIsConfigured(
        string $failureMessage,
        $status = BudgetDemandService::DISCARDED_STATUS
    )
    {
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getId')
                ->willReturn(1);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getStatus')
                ->willReturn($status);

        $this->budgetDemandResponseModelMock
                ->method('getId')
                ->willReturn(null);
        $this->budgetDemandResponseModelMock
                ->method('getSuccess')
                ->willReturn(false);
        $this->budgetDemandResponseModelMock
                ->expects($this->once())
                ->method('getError')
                ->willReturn($failureMessage);
    }

    private function andSetFailureDueToPersistingBudgetDemandStepIsConfigured()
    {
        $this->entityManagerInterfaceMock
                ->expects($this->once())
                ->method('persist')
                ->will($this->throwException(new \Exception));
    }

    private function whenDiscardIsCalled(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel): BudgetDemandResponseModel
    {
        return $this->budgetDemandService->discard($budgetDemandUpdateRequestModel);
    }

    private function thenTheBudgetDemandEntityHasBeenDiscarded(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat($this->budgetDemandResponseModelMock, $this->equalTo($budgetDemandResponseModel));
    }

    private function thenTheBudgetDemandEntityHasNotBeenDiscarded(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat(self::ERROR_MESSAGE_GENERAL, $this->equalTo($budgetDemandResponseModel->getError()));
    }

    private function thenTheBudgetDemandEntityFailsDueToStatusIsAlreadyDiscarded(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat(BudgetDemandService::BUDGET_DEMAND_ALREADY_DISCARDED, $this->equalTo($budgetDemandResponseModel->getError()));
    }

}
