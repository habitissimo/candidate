<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Serializer\SerializerInterface;

use Doctrine\ORM\EntityManagerInterface;

use \App\Service\ValidateService;

use \App\Model\BudgetDemandUpdateRequestModel;
use \App\Entity\BudgetDemand;
use \App\Model\BudgetDemandResponseModel;

use \App\Service\BudgetDemandService;
use \App\Repository\BudgetDemandRepository;

class BudgetDemandServicePublishTest extends TestCase
{

    private const ERROR_MESSAGE_GENERAL = "Error persisting data.";
    private const DEFAULT_TITLE = "Error persisting data.";
    private const DEFAULT_CATEGORY = "Error persisting data.";

    private $entityManagerInterfaceMock;
    private $validateServiceMock;
    private $serializerInterfaceMock;
    private $budgetDemandResponseModelMock;
    private $budgetDemandMock;
    private $BudgetDemandRepositoryMock;

    private $budgetDemandService;

    /** before */
    public function setUp()
    {
        $this->entityManagerInterfaceMock = $this->createMock(EntityManagerInterface::class);
        $this->validateServiceMock = $this->createMock(ValidateService::class);
        $this->serializerInterfaceMock = $this->createMock(SerializerInterface::class);
        $this->budgetDemandResponseModelMock = $this->createMock(BudgetDemandResponseModel::class);
        $this->budgetDemandMock = $this->createMock(BudgetDemand::class);
        $this->BudgetDemandRepositoryMock = $this->createMock(BudgetDemandRepository::class);

        $this->budgetDemandService = new BudgetDemandService(
            $this->entityManagerInterfaceMock,
            $this->validateServiceMock,
            $this->serializerInterfaceMock,
            $this->budgetDemandResponseModelMock
        );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function publishShouldSuccess(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel, $data)
    {
        $this->givenValidateMethodStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetSuccessStepIsConfigured($budgetDemandUpdateRequestModel);
        $budgetDemandResponseModel = $this->whenPublishIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasBeenPublished($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function publishShouldFailsDueToExceptionPersistingBudgetDemand(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        $data
    )
    {
        $this->givenValidateMethodStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetFailureStepIsConfigured(
            self::ERROR_MESSAGE_GENERAL,
            BudgetDemandService::PENDING_STATUS,
            self::DEFAULT_TITLE,
            self::DEFAULT_CATEGORY
        );
        $this->andSetFailureDueToPersistingBudgetDemandStepIsConfigured();
        $budgetDemandResponseModel = $this->whenPublishIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasNotBeenPublished($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }
    
    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function publishShouldFailsDueToNotFoundBudgetDemandIntoDB(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        $data
    )
    {
        $this->givenValidateMethodStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsNotFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetFailureStepIsConfigured(
            BudgetDemandService::BUDGET_DEMAND_WAS_NOT_FOUND . $budgetDemandUpdateRequestModel->getId(),
            BudgetDemandService::PENDING_STATUS,
            self::DEFAULT_TITLE,
            self::DEFAULT_CATEGORY
        );

        $budgetDemandResponseModel = $this->whenPublishIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasNotBeenUpdatedDueToNotFoundIntoDB($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function publishShouldFailsDueToBudgetDemandStatusIsNotPending(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        $data
    )
    {
        $this->givenValidateMethodStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetFailureStepIsConfigured(
            BudgetDemandService::BUDGET_DEMAND_CAN_NOT_BE_MODIFIED,
            BudgetDemandService::PUBLISHED_STATUS,
            self::DEFAULT_TITLE,
            self::DEFAULT_CATEGORY
        );

        $budgetDemandResponseModel = $this->whenPublishIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasNotBeenUpdatedDueToStatusIsNotPending($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function publishShouldFailsDueToTitleIsNotSet(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        $data
    )
    {
        $this->givenValidateMethodStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetFailureStepIsConfigured(
            BudgetDemandService::BUDGET_DEMAND_CAN_NOT_BE_PUBLISH_DUE_TO_TITLE,
            BudgetDemandService::PENDING_STATUS
        );

        $budgetDemandResponseModel = $this->whenPublishIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasNotBeenUpdatedDueToTitleIsNotSetOrBlank($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function publishShouldFailsDueToTitleIsBlank(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        $data
    )
    {
        $this->givenValidateMethodStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetFailureStepIsConfigured(
            BudgetDemandService::BUDGET_DEMAND_CAN_NOT_BE_PUBLISH_DUE_TO_TITLE,
            BudgetDemandService::PENDING_STATUS,
            "   ",
            self::DEFAULT_CATEGORY
        );

        $budgetDemandResponseModel = $this->whenPublishIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasNotBeenUpdatedDueToTitleIsNotSetOrBlank($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function publishShouldFailsDueToCategoryIsNotSet(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        $data
    )
    {
        $this->givenValidateMethodStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetFailureStepIsConfigured(
            BudgetDemandService::BUDGET_DEMAND_CAN_NOT_BE_PUBLISH_DUE_TO_CATEGORY,
            BudgetDemandService::PENDING_STATUS,
            self::DEFAULT_TITLE
        );

        $budgetDemandResponseModel = $this->whenPublishIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasNotBeenUpdatedDueToCategoryIsNotSetOrBlank($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function publishShouldFailsDueToCategoryIsBlank(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        $data
    )
    {
        $this->givenValidateMethodStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetFailureStepIsConfigured(
            BudgetDemandService::BUDGET_DEMAND_CAN_NOT_BE_PUBLISH_DUE_TO_CATEGORY,
            BudgetDemandService::PENDING_STATUS,
            self::DEFAULT_TITLE,
            "   "
        );

        $budgetDemandResponseModel = $this->whenPublishIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasNotBeenUpdatedDueToCategoryIsNotSetOrBlank($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    private function givenValidateMethodStepIsConfigured(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $this->validateServiceMock
                ->expects($this->once())
                ->method('validate')
                ->with($budgetDemandUpdateRequestModel)
                ->willReturn($budgetDemandUpdateRequestModel);
    }

    private function andIdIsFoundIntoDBStepIsConfigured(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $this->configBudgetDemandRepositoryMock($budgetDemandUpdateRequestModel, $this->budgetDemandMock);
    }

    private function andIdIsNotFoundIntoDBStepIsConfigured(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $this->configBudgetDemandRepositoryMock($budgetDemandUpdateRequestModel, null);
    }

    private function configBudgetDemandRepositoryMock(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel, $returnObject)
    {
        $this->entityManagerInterfaceMock
                ->expects($this->once())
                ->method('getRepository')
                ->with('\App\Entity\BudgetDemand')
                ->willReturn($this->BudgetDemandRepositoryMock);

        $this->BudgetDemandRepositoryMock
                ->expects($this->once())
                ->method('find')
                ->with($budgetDemandUpdateRequestModel->getId())
                ->willReturn($returnObject);
    }

    private function andSetSuccessStepIsConfigured(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getId')
                ->willReturn(1);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getStatus')
                ->willReturn(BudgetDemandService::PENDING_STATUS);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getDescription')
                ->willReturn($budgetDemandUpdateRequestModel->getDescription());
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getTitle')
                ->willReturn($budgetDemandUpdateRequestModel->getTitle());
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getCategory')
                ->willReturn($budgetDemandUpdateRequestModel->getCategory());
    }

    private function andSetFailureStepIsConfigured(
        string $failureMessage,
        $status = BudgetDemandService::PENDING_STATUS,
        $title = null,
        $category = null
    )
    {
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getId')
                ->willReturn(1);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getStatus')
                ->willReturn($status);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getDescription')
                ->willReturn("A sort Description");
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getTitle')
                ->willReturn($title);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getCategory')
                ->willReturn($category);

        $this->budgetDemandResponseModelMock
                ->method('getId')
                ->willReturn(null);
        $this->budgetDemandResponseModelMock
                ->method('getSuccess')
                ->willReturn(false);
        $this->budgetDemandResponseModelMock
                ->expects($this->once())
                ->method('getError')
                ->willReturn($failureMessage);
    }

    private function andSetFailureDueToPersistingBudgetDemandStepIsConfigured()
    {
        $this->entityManagerInterfaceMock
                ->expects($this->once())
                ->method('persist')
                ->will($this->throwException(new \Exception));
    }

    private function whenPublishIsCalled(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel): BudgetDemandResponseModel
    {
        return $this->budgetDemandService->publish($budgetDemandUpdateRequestModel);
    }

    private function thenTheBudgetDemandEntityHasBeenPublished(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat($this->budgetDemandResponseModelMock, $this->equalTo($budgetDemandResponseModel));
    }

    private function thenTheBudgetDemandEntityHasNotBeenPublished(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat(self::ERROR_MESSAGE_GENERAL, $this->equalTo($budgetDemandResponseModel->getError()));
    }

    private function thenTheBudgetDemandEntityHasNotBeenUpdatedDueToNotFoundIntoDB(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat(
            BudgetDemandService::BUDGET_DEMAND_WAS_NOT_FOUND . $budgetDemandUpdateRequestModel->getId(),
            $this->equalTo($budgetDemandResponseModel->getError())
        );
    }

    private function thenTheBudgetDemandEntityHasNotBeenUpdatedDueToStatusIsNotPending(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat(
            BudgetDemandService::BUDGET_DEMAND_CAN_NOT_BE_MODIFIED,
            $this->equalTo($budgetDemandResponseModel->getError())
        );
    }

    private function thenTheBudgetDemandEntityHasNotBeenUpdatedDueToTitleIsNotSetOrBlank(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat(
            BudgetDemandService::BUDGET_DEMAND_CAN_NOT_BE_PUBLISH_DUE_TO_TITLE,
            $this->equalTo($budgetDemandResponseModel->getError())
        );
    }

    private function thenTheBudgetDemandEntityHasNotBeenUpdatedDueToCategoryIsNotSetOrBlank(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat(
            BudgetDemandService::BUDGET_DEMAND_CAN_NOT_BE_PUBLISH_DUE_TO_CATEGORY,
            $this->equalTo($budgetDemandResponseModel->getError())
        );
    }

}
