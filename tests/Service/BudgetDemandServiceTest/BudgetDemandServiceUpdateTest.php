<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Serializer\SerializerInterface;

use Doctrine\ORM\EntityManagerInterface;

use \App\Service\ValidateService;

use \App\Model\BudgetDemandUpdateRequestModel;
use \App\Entity\BudgetDemand;
use \App\Model\BudgetDemandResponseModel;

use \App\Service\BudgetDemandService;
use \App\Repository\BudgetDemandRepository;

class BudgetDemandServiceUpdateTest extends TestCase
{

    private const ERROR_MESSAGE_GENERAL = "Error persisting data.";
    
    private $entityManagerInterfaceMock;
    private $validateServiceMock;
    private $serializerInterfaceMock;
    private $budgetDemandResponseModelMock;
    private $budgetDemandMock;
    private $BudgetDemandRepositoryMock;

    private $budgetDemandService;

    /** before */
    public function setUp()
    {
        $this->entityManagerInterfaceMock = $this->createMock(EntityManagerInterface::class);
        $this->validateServiceMock = $this->createMock(ValidateService::class);
        $this->serializerInterfaceMock = $this->createMock(SerializerInterface::class);
        $this->budgetDemandResponseModelMock = $this->createMock(BudgetDemandResponseModel::class);
        $this->budgetDemandMock = $this->createMock(BudgetDemand::class);
        $this->BudgetDemandRepositoryMock = $this->createMock(BudgetDemandRepository::class);

        $this->budgetDemandService = new BudgetDemandService(
            $this->entityManagerInterfaceMock,
            $this->validateServiceMock,
            $this->serializerInterfaceMock,
            $this->budgetDemandResponseModelMock
        );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function updateShouldSuccess(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel, $data)
    {
        $this->givenValidateStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetSuccessStepIsConfigured($budgetDemandUpdateRequestModel);
        $budgetDemandResponseModel = $this->whenUpdateIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasBeenUpdated($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }
    
    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function updateShouldFailsDueToExceptionPersistingBudgetDemand(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        $data
    )
    {
        $this->givenValidateStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetFailureStepIsConfigured("Error persisting data.", BudgetDemandService::PENDING_STATUS);
        $this->andSetFailurePersistingBudgetDemandStepIsConfigured();
        $budgetDemandResponseModel = $this->whenUpdateIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasNotBeenUpdated($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function updateShouldFailsDueToNotFoundBudgetDemandIntoDB(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        $data
    )
    {
        $this->givenValidateStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsNotFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetFailureStepIsConfigured(
            BudgetDemandService::BUDGET_DEMAND_WAS_NOT_FOUND . $budgetDemandUpdateRequestModel->getId(),
            BudgetDemandService::PENDING_STATUS
        );

        $budgetDemandResponseModel = $this->whenUpdateIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasNotBeenUpdatedDueToNotFoundIntoDB($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function updateShouldFailsDueToBudgetDemandStatusIsNotPending(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        $data
    )
    {
        $this->givenValidateStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andIdIsFoundIntoDBStepIsConfigured($budgetDemandUpdateRequestModel);
        $this->andSetFailureStepIsConfigured(
            BudgetDemandService::BUDGET_DEMAND_CAN_NOT_BE_MODIFIED,
            BudgetDemandService::PUBLISHED_STATUS
        );

        $budgetDemandResponseModel = $this->whenUpdateIsCalled($budgetDemandUpdateRequestModel);
        $this->thenTheBudgetDemandEntityHasNotBeenUpdatedDueToStatusIsNotPending($budgetDemandUpdateRequestModel, $budgetDemandResponseModel);
    }

    private function givenValidateStepIsConfigured(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $this->validateServiceMock
                ->expects($this->once())
                ->method('validate')
                ->with($budgetDemandUpdateRequestModel)
                ->willReturn($budgetDemandUpdateRequestModel);
    }

    private function andIdIsFoundIntoDBStepIsConfigured(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $this->entityManagerInterfaceMock
                ->expects($this->once())
                ->method('getRepository')
                ->with('\App\Entity\BudgetDemand')
                ->willReturn($this->BudgetDemandRepositoryMock);

        $this->BudgetDemandRepositoryMock
                ->expects($this->once())
                ->method('find')
                ->with($budgetDemandUpdateRequestModel->getId())
                ->willReturn($this->budgetDemandMock);
    }

    private function andIdIsNotFoundIntoDBStepIsConfigured(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $this->entityManagerInterfaceMock
                ->expects($this->once())
                ->method('getRepository')
                ->with('\App\Entity\BudgetDemand')
                ->willReturn($this->BudgetDemandRepositoryMock);

        $this->BudgetDemandRepositoryMock
                ->expects($this->once())
                ->method('find')
                ->with($budgetDemandUpdateRequestModel->getId())
                ->willReturn(null);
    }

    private function andSetSuccessStepIsConfigured(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
        $description = $budgetDemandUpdateRequestModel->getDescription() || "";
        $title = $budgetDemandUpdateRequestModel->getTitle() || "";
        $category = $budgetDemandUpdateRequestModel->getCategory() || "";

        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getId')
                ->willReturn(1);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getStatus')
                ->willReturn(BudgetDemandService::PENDING_STATUS);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getDescription')
                ->willReturn($description);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getTitle')
                ->willReturn($title);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getCategory')
                ->willReturn($category);
    }

    private function andSetFailureStepIsConfigured(string $failureMessage, $status = BudgetDemandService::PENDING_STATUS)
    {
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getId')
                ->willReturn(1);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getStatus')
                ->willReturn($status);
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getDescription')
                ->willReturn("");
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getTitle')
                ->willReturn("");
        $this->budgetDemandMock
                ->expects($this->any())
                ->method('getCategory')
                ->willReturn("");

        $this->budgetDemandResponseModelMock
                ->method('getId')
                ->willReturn(null);
        $this->budgetDemandResponseModelMock
                ->method('getSuccess')
                ->willReturn(false);
        $this->budgetDemandResponseModelMock
                ->expects($this->once())
                ->method('getError')
                ->willReturn($failureMessage);
    }

    private function andSetFailurePersistingBudgetDemandStepIsConfigured()
    {
        $this->entityManagerInterfaceMock
                ->expects($this->once())
                ->method('persist')
                ->will($this->throwException(new \Exception));
    }

    private function whenUpdateIsCalled(BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel): BudgetDemandResponseModel
    {
        return $this->budgetDemandService->update($budgetDemandUpdateRequestModel);
    }

    private function thenTheBudgetDemandEntityHasBeenUpdated(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat($this->budgetDemandResponseModelMock, $this->equalTo($budgetDemandResponseModel));
    }

    private function thenTheBudgetDemandEntityHasNotBeenUpdated(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat(self::ERROR_MESSAGE_GENERAL, $this->equalTo($budgetDemandResponseModel->getError()));
    }

    private function thenTheBudgetDemandEntityHasNotBeenUpdatedDueToNotFoundIntoDB(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat(
            BudgetDemandService::BUDGET_DEMAND_WAS_NOT_FOUND . $budgetDemandUpdateRequestModel->getId(),
            $this->equalTo($budgetDemandResponseModel->getError())
        );
    }

    private function thenTheBudgetDemandEntityHasNotBeenUpdatedDueToStatusIsNotPending(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        BudgetDemandResponseModel $budgetDemandResponseModel
    )
    {
        $this->assertThat(
            BudgetDemandService::BUDGET_DEMAND_CAN_NOT_BE_MODIFIED,
            $this->equalTo($budgetDemandResponseModel->getError())
        );
    }

}
