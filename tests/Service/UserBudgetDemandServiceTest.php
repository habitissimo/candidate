<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Service\UserService;
use \App\Service\BudgetDemandService;
use \App\Model\UserRequestModel;
use \App\Model\BudgetDemandCreateRequestModel;
use \App\Model\UserResponseModel;
use \App\Model\BudgetDemandResponseModel;
use \App\Model\UserBudgetDemandCreateRequestModel;
use \App\Model\UserBudgetDemandResponseModel;

use \App\Service\UserBudgetDemandService;

class UserBudgetDemandServiceTest extends TestCase
{

    const USER_ID_DEFAULT = 354;
    const BUDGET_DEMAND_ID_DEFAULT = 1543;
    const ERROR_USER_DEFAULT = "Error generated by UserRequest";
    const ERROR_BUDGET_DEMAND_DEFAULT = "Error generated by BudgetDemandRequest";

    private $userServiceMock;
    private $budgetDemandServiceMock;
    private $userRequestModelMock;
    private $budgetDemandCreateRequestModelMock;
    private $userResponseModelMock;
    private $budgetDemandResponseModel;
    private $userBudgetDemandResponseModelMock;
    
    private $userBudgetDemandService;
    
    /** before */
    public function setUp()
    {
        $this->userServiceMock = $this->createMock(UserService::class);
        $this->budgetDemandServiceMock = $this->createMock(BudgetDemandService::class);

        $this->userRequestModelMock = $this->createMock(UserRequestModel::class);
        $this->budgetDemandCreateRequestModelMock = $this->createMock(BudgetDemandCreateRequestModel::class);

        $this->userResponseModelMock = $this->createMock(UserResponseModel::class);
        $this->budgetDemandResponseModelMock = $this->createMock(BudgetDemandResponseModel::class);
        $this->userBudgetDemandResponseModelMock = $this->createMock(UserBudgetDemandResponseModel::class);

        $this->userBudgetDemandService = new UserBudgetDemandService(
            $this->userServiceMock,
            $this->budgetDemandServiceMock,
            $this->userRequestModelMock,
            $this->budgetDemandCreateRequestModelMock,
            $this->userBudgetDemandResponseModelMock
        );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserBudgetDemandDataProvider::validUserBudgetDemandCreateRequestModel
    */
    public function userCreateOrUpdateAndBudgetDemandCreateShouldSuccess(UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel)
    {
        $this->givenSetUserModelStepIsConfigured($userBudgetDemandCreateRequestModel);
        $this->andSetBudgetDemandCreateModelStepIsConfigured($userBudgetDemandCreateRequestModel);
        $this->andUserServiceCreateOrUpdateStepIsConfigured(self::USER_ID_DEFAULT, true);
        $this->andBudgetDemandServiceCreateStepIsConfigured(self::BUDGET_DEMAND_ID_DEFAULT, true);
        $this->andSetUserBudgetDemandResponseModelStepIsConfigured(true);

        $userBudgetDemandResponseModel = $this->whenUserCreateOrUpdateAndBudgetDemandCreate($userBudgetDemandCreateRequestModel);
        $this->thenUserCreateOrUpdateAndBudgetDemandCreateIsSuccess($userBudgetDemandCreateRequestModel, $userBudgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserBudgetDemandDataProvider::invalidUserRequestModel
    */
    public function userCreateOrUpdateAndBudgetDemandCreateShouldFailsDueToUserRequest(
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel
    )
    {
        $this->givenSetUserModelStepIsConfigured($userBudgetDemandCreateRequestModel);
        $this->andSetBudgetDemandCreateModelStepIsConfigured($userBudgetDemandCreateRequestModel);
        $this->andUserServiceCreateOrUpdateStepIsConfigured(null, false, self::ERROR_USER_DEFAULT);
        $this->andBudgetDemandServiceCreateStepIsConfigured(self::BUDGET_DEMAND_ID_DEFAULT, true);
        $this->andSetUserBudgetDemandResponseModelStepIsConfigured(false);

        $userBudgetDemandResponseModel = $this->whenUserCreateOrUpdateAndBudgetDemandCreate($userBudgetDemandCreateRequestModel);
        $this->thenUserCreateOrUpdateAndBudgetDemandCreateFailsDueToUserRequest($userBudgetDemandCreateRequestModel, $userBudgetDemandResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserBudgetDemandDataProvider::invalidBudgetDemandCreateRequestModel
    */
    public function userCreateOrUpdateAndBudgetDemandCreateShouldFailsDueToBudgetDemandRequest(
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel
    )
    {
        $this->givenSetUserModelStepIsConfigured($userBudgetDemandCreateRequestModel);
        $this->andSetBudgetDemandCreateModelStepIsConfigured($userBudgetDemandCreateRequestModel);
        $this->andUserServiceCreateOrUpdateStepIsConfigured(self::USER_ID_DEFAULT, true);
        $this->andBudgetDemandServiceCreateStepIsConfigured(null, false, self::ERROR_BUDGET_DEMAND_DEFAULT);
        $this->andSetUserBudgetDemandResponseModelStepIsConfigured(false);

        $userBudgetDemandResponseModel = $this->whenUserCreateOrUpdateAndBudgetDemandCreate($userBudgetDemandCreateRequestModel);
        $this->thenUserCreateOrUpdateAndBudgetDemandCreateFailsDueToBudgetDemandRequest($userBudgetDemandCreateRequestModel, $userBudgetDemandResponseModel);
    }

    private function givenSetUserModelStepIsConfigured(UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel)
    {
        $this->userRequestModelMock
                ->method('getEmail')
                ->willReturn($userBudgetDemandCreateRequestModel->getEmail());
        $this->userRequestModelMock
                ->method('getTelephone')
                ->willReturn($userBudgetDemandCreateRequestModel->getTelephone());
        $this->userRequestModelMock
                ->method('getAddress')
                ->willReturn($userBudgetDemandCreateRequestModel->getAddress());
    }

    private function andSetBudgetDemandCreateModelStepIsConfigured(UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel)
    {
        $this->budgetDemandCreateRequestModelMock
                ->method('getEmail')
                ->willReturn($userBudgetDemandCreateRequestModel->getEmail());
        $this->budgetDemandCreateRequestModelMock
                ->method('getTitle')
                ->willReturn($userBudgetDemandCreateRequestModel->getTitle());
        $this->budgetDemandCreateRequestModelMock
                ->method('getDescription')
                ->willReturn($userBudgetDemandCreateRequestModel->getDescription());
        $this->budgetDemandCreateRequestModelMock
                ->method('getCategory')
                ->willReturn($userBudgetDemandCreateRequestModel->getCategory());
    }

    private function andUserServiceCreateOrUpdateStepIsConfigured($id = null, $success = false, $error = null)
    {
        $userResponseModelReal = (new UserResponseModel())
                                        ->setId($id)
                                        ->setSuccess($success)
                                        ->setError($error);

        $this->userServiceMock
                ->expects($this->once())
                ->method('createOrUpdate')
                ->with($this->userRequestModelMock)
                ->willReturn($userResponseModelReal);

        $this->userResponseModelMock
                ->method('getId')
                ->willReturn($id);
        $this->userResponseModelMock
                ->method('getSuccess')
                ->willReturn($success);
        $this->userResponseModelMock
                ->method('getError')
                ->willReturn($error);
    }

    private function andBudgetDemandServiceCreateStepIsConfigured($id = null, $success = false, $error = null)
    {
        $budgetDemandResponseModelReal = (new BudgetDemandResponseModel())
                                                ->setId($id)
                                                ->setSuccess($success)
                                                ->setError($error);

        $this->budgetDemandServiceMock
                ->expects($this->once())
                ->method('create')
                ->with($this->budgetDemandCreateRequestModelMock)
                ->willReturn($budgetDemandResponseModelReal);

        $this->budgetDemandResponseModelMock
                ->method('getId')
                ->willReturn($id);
        $this->budgetDemandResponseModelMock
                ->method('getSuccess')
                ->willReturn($success);
        $this->budgetDemandResponseModelMock
                ->method('getError')
                ->willReturn($error);
    }

    private function andSetUserBudgetDemandResponseModelStepIsConfigured($success = false)
    {
        $this->userBudgetDemandResponseModelMock
                ->method('getSuccess')
                ->willReturn($success);

        $this->userBudgetDemandResponseModelMock
                ->method('getUser')
                ->willReturn($this->userResponseModelMock);

        $this->userBudgetDemandResponseModelMock
                ->method('getBudgetDemand')
                ->willReturn($this->budgetDemandResponseModelMock);
    }

    private function whenUserCreateOrUpdateAndBudgetDemandCreate(
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel
    )
    {
        return $this->userBudgetDemandService->userCreateOrUpdateAndBudgetDemandCreate($userBudgetDemandCreateRequestModel);
    }

    private function thenUserCreateOrUpdateAndBudgetDemandCreateIsSuccess(
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel,
        UserBudgetDemandResponseModel $userBudgetDemandResponseModel
    )
    {
        $this->assertThat($this->userBudgetDemandResponseModelMock, $this->equalTo($userBudgetDemandResponseModel));
        $this->assertThat($this->userResponseModelMock, $this->equalTo($userBudgetDemandResponseModel->getUser()));
        $this->assertThat($this->budgetDemandResponseModelMock, $this->equalTo($userBudgetDemandResponseModel->getBudgetDemand()));
    }

    private function thenUserCreateOrUpdateAndBudgetDemandCreateFailsDueToUserRequest(
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel,
        UserBudgetDemandResponseModel $userBudgetDemandResponseModel
    )
    {
        $this->assertThat(
            $this->userResponseModelMock->getError(),
            $this->equalTo($userBudgetDemandResponseModel->getUser()->getError())
        );
    }

    private function thenUserCreateOrUpdateAndBudgetDemandCreateFailsDueToBudgetDemandRequest(
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel,
        UserBudgetDemandResponseModel $userBudgetDemandResponseModel
    )
    {
        $this->assertThat(
            $this->budgetDemandResponseModelMock->getError(),
            $this->equalTo($userBudgetDemandResponseModel->getBudgetDemand()->getError())
        );
    }

}
