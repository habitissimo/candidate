<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationList;

use \App\Service\ValidateService;

class ValidateServiceTest extends TestCase
{

    private $validatorInterfaceMock;
    private $constraintViolationListMock;

    private $validateService;
    
    /** before */
    public function setUp()
    {
        $this->constraintViolationLisMock = $this->createMock(ConstraintViolationList::class);
        $this->validatorInterfaceMock = $this->createMock(ValidatorInterface::class);

        $this->validateService = new ValidateService($this->validatorInterfaceMock);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\ValidateDataProvider::validObjects
    */
    public function validateShouldNotThrowException($object)
    {
        $this->givenValidateStepIsConfigured(null);

        $objectValidated = $this->validateService->validate($object);
        $this->assertThat($objectValidated, $this->equalTo($object));
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\ValidateDataProvider::invalidObjects
     * @expectedExcetion    \Excetion
    */
    public function validateShouldThrowException($object)
    {
        $this->givenValidateStepIsConfigured($this->constraintViolationLisMock);
        $this->validateService->validate($object);
    }

    private function givenValidateStepIsConfigured($returnObject)
    {
        $this->validatorInterfaceMock
                ->expects($this->once())
                ->method('validate')
                ->willReturn($returnObject);
    }

}
