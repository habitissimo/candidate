<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

use Doctrine\ORM\EntityManagerInterface;

use \App\Model\UserRequestModel;
use \App\Entity\User;
use \App\Model\UserResponseModel;

use \App\Repository\UserRepository;
use \App\Service\UserService;

class UserServiceTest extends TestCase
{

    private $requestMock;
    private $serializerInterfaceMock;
    private $entityManagerInterfaceMock;
    private $userMock;
    private $userResponseModelMock;
    
    private $userRepositoryMock;
    private $userService;
    
    /** before */
    public function setUp()
    {
        $this->requestMock = $this->createMock(Request::class);
        $this->serializerInterfaceMock = $this->createMock(SerializerInterface::class);
        $this->entityManagerInterfaceMock = $this->createMock(EntityManagerInterface::class);
        $this->userMock = $this->createMock(User::class);
        $this->userResponseModelMock = $this->createMock(UserResponseModel::class);

        $this->userRepositoryMock = $this->createMock(UserRepository::class);
        $this->userService = new UserService(
                                    $this->entityManagerInterfaceMock,
                                    $this->serializerInterfaceMock,
                                    $this->userResponseModelMock
                                );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserDataProvider::validUserRequestModel
    */
    public function createOrUpdateShouldSuccessCreate(
        UserRequestModel $userRequestModel,
        string $userRequestModelSerializedHelper
    )
    {
        $this->givenGetEntityMethodStepIsConfigured($userRequestModel, $userRequestModelSerializedHelper);
        $this->andCreateOrUpdateStrategyStepIsConfigured();
        $this->andEntityWasNotFoundIntoDBStepIsConfigured();
        $this->andSetSuccessStepIsConfigured();
        $userResponseModel = $this->whenCreateOrUpdateIsCalled($userRequestModel);
        $this->thenTheUserEntityHasBeenCreated($userRequestModel, $userResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserDataProvider::validUserRequestModel
    */
    public function createOrUpdateShouldSuccessUpdate(UserRequestModel $userRequestModel, string $userRequestModelSerializedHelper)
    {
        $this->givenGetEntityMethodStepIsConfigured($userRequestModel, $userRequestModelSerializedHelper);
        $this->andCreateOrUpdateStrategyStepIsConfigured();
        $this->andEntityFoundIntoDBStepIsConfigured();
        $this->andSetSuccessStepIsConfigured();
        $userResponseModel = $this->whenCreateOrUpdateIsCalled($userRequestModel);
        $this->thenTheUserEntityHasBeenCreated($userRequestModel, $userResponseModel);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserDataProvider::validUserRequestModel
    */
    public function createOrUpdateShouldFails(UserRequestModel $userRequestModel, string $userRequestModelSerializedHelper)
    {
        $this->givenGetEntityMethodStepIsConfigured($userRequestModel, $userRequestModelSerializedHelper);
        $this->andCreateOrUpdateStrategyStepIsConfigured();
        $this->andEntityFoundIntoDBStepIsConfigured();
        $this->andSetFailureStepIsConfigured();
        $userResponseModel = $this->whenCreateOrUpdateIsCalled($userRequestModel);
        $this->thenTheUserEntityHasNotBeenCreated($userRequestModel, $userResponseModel);
    }

    private function givenGetEntityMethodStepIsConfigured(UserRequestModel $userRequestModel, string $userRequestModelSerializedHelper)
    {
        $this->userMock
                ->method('getTelephone')
                ->willReturn($userRequestModel->getTelephone());
        $this->userMock
                ->method('getAddress')
                ->willReturn($userRequestModel->getAddress());
        $this->userMock
                ->method('getEmail')
                ->willReturn($userRequestModel->getEmail());

        $this->serializerInterfaceMock
                ->expects($this->once())
                ->method('serialize')
                ->with(
                    $userRequestModel,
                    'json'
                )
                ->willReturn($userRequestModelSerializedHelper);

        $this->serializerInterfaceMock
                ->expects($this->once())
                ->method('deserialize')
                ->with(
                    $userRequestModelSerializedHelper,
                    User::class,
                    'json'
                )
                ->willReturn($this->userMock);
    }

    private function andCreateOrUpdateStrategyStepIsConfigured()
    {
        $this->entityManagerInterfaceMock
                ->expects($this->once())
                ->method('getRepository')
                ->with('\App\Entity\User')
                ->willReturn($this->userRepositoryMock);
    }

    private function andEntityWasNotFoundIntoDBStepIsConfigured()
    {
        $this->userRepositoryMock
                ->expects($this->once())
                ->method('findOneBy')
                ->with(['email' =>  $this->userMock->getEmail()])
                ->willReturn(null);

        $this->userMock
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);
    }
    
    private function andEntityFoundIntoDBStepIsConfigured()
    {
        $this->userRepositoryMock
                ->expects($this->once())
                ->method('findOneBy')
                ->with(['email' =>  $this->userMock->getEmail()])
                ->willReturn($this->userMock);

        $this->userMock
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);
    }

    private function andSetSuccessStepIsConfigured()
    {
        $this->userResponseModelMock
                ->method('getId')
                ->willReturn($this->userMock->getId());
        $this->userResponseModelMock
                ->expects($this->once())
                ->method('getSuccess')
                ->willReturn(true);
        $this->userResponseModelMock
                ->method('getError')
                ->willReturn(null);
    }

    private function andSetFailureStepIsConfigured()
    {
        $this->entityManagerInterfaceMock
                ->expects($this->once())
                ->method('persist')
                ->will($this->throwException(new \Exception));
    }

    private function whenCreateOrUpdateIsCalled(UserRequestModel $userRequestModel)
    {
        return $this->userService->createOrUpdate($userRequestModel);
    }
    
    private function thenTheUserEntityHasBeenCreated(UserRequestModel $userRequestModel, UserResponseModel $userResponseModel)
    {
        $this->assertThat(true, $this->equalTo($userResponseModel->getSuccess()));
    }

    private function thenTheUserEntityHasNotBeenCreated(UserRequestModel $userRequestModel, UserResponseModel $userResponseModel)
    {
        $this->assertThat(false, $this->equalTo($userResponseModel->getSuccess()));
    }
}
