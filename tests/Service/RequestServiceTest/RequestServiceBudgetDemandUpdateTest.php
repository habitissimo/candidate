<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

use \App\Model\BudgetDemandUpdateRequestModel;

use \App\Service\RequestService;

class RequestServiceBudgetDemandUpdateTest extends TestCase
{

    private $serializerInterfaceMock;
    private $requestMock;

    /** before */
    public function setUp()
    {
        $this->serializerInterfaceMock = $this->createMock(SerializerInterface::class);
        $this->requestMock = $this->createMock(Request::class);

        $this->requestService = new RequestService(
            $this->serializerInterfaceMock
        );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\RequestDataProvider::validBudgetDemandUpdateRequestModel
     */
    public function getBudgetDemandUpdateShouldReturnBudgetDemandUpdateRequestModel(
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel,
        string $content
    )
    {
        $this->givenRequestContentIsSetStepIsConfigured($content);
        $this->andGetModelStepIsConfigured(
            $content,
            BudgetDemandUpdateRequestModel::class,
            $budgetDemandUpdateRequestModel
        );
        $budgetDemandUpdateRequestModelResponse = $this->whenGetBudgetDemandUpdateIsCalled($this->requestMock);
        $this->thenGetBudgetDemandUpdateHasBeenUpdated($content, $budgetDemandUpdateRequestModelResponse);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function getBudgetDemandUpdateShouldThrowException()
    {
        $this->givenRequestContentThrowsExceptionStepIsConfgured();
        $this->whenGetBudgetDemandUpdateIsCalled($this->requestMock);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\RequestDataProvider::invalidDeserialization
     * @expectedException \Exception
     */
    public function serializationShouldThrowExcetionDueToBadClass($request, string $class)
    {
        $this->whenDeserializeThenThrowExceptionDueToBadClass($request, $class);
    }

    private function givenRequestContentIsSetStepIsConfigured(string $content)
    {
        $this->requestMock
                ->expects($this->once())
                ->method('getContent')
                ->willReturn($content);
    }

    private function givenRequestContentThrowsExceptionStepIsConfgured()
    {
        $this->requestMock
                ->expects($this->once())
                ->method('getContent')
                ->will($this->throwException(new \Exception));
    }

    private function andGetModelStepIsConfigured(
        string $content,
        string $clazz,
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel
    )
    {
        $this->serializerInterfaceMock
                ->expects($this->once())
                ->method('deserialize')
                ->with(
                    $content,
                    $clazz,
                    'json',
                    array('allow_extra_attributes' => false)
                )
                ->willReturn($budgetDemandUpdateRequestModel);
    }

    private function whenDeserializeThenThrowExceptionDueToBadClass(
        $request,
        string $clazz
    )
    {
        if($clazz !== get_class($request)) {
            throw new \Exception();
        }
    }

    private function whenGetBudgetDemandUpdateIsCalled(Request $request): BudgetDemandUpdateRequestModel
    {
        return $this->requestService->getBudgetDemandUpdate($request);
    }

    private function thenGetBudgetDemandUpdateHasBeenUpdated(
        string $content,
        BudgetDemandUpdateRequestModel $budgetDemandUpdateRequestModel)
    {
       $obj = json_decode($content);
       $this->assertThat($obj->id, $this->equalTo($budgetDemandUpdateRequestModel->getId()));
    }

}
