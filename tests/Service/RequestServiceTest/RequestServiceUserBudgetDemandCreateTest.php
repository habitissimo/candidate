<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

use \App\Model\UserBudgetDemandCreateRequestModel;

use \App\Service\RequestService;

class RequestServiceUserBudgetDemandCreateTest extends TestCase
{

    private $serializerInterfaceMock;
    private $requestMock;

    /** before */
    public function setUp()
    {
        $this->serializerInterfaceMock = $this->createMock(SerializerInterface::class);
        $this->requestMock = $this->createMock(Request::class);

        $this->requestService = new RequestService(
            $this->serializerInterfaceMock
        );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\RequestDataProvider::validUserBudgetDemandCreateRequestModel
     */
    public function getUserBudgetDemandCreateShouldReturnUserBudgetDemandCreateRequestModel(
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel,
        string $content
    )
    {
        $this->givenRequestContentIsSetStepIsConfigured($content);
        $this->andGetModelStepIsConfigured(
            $content,
            UserBudgetDemandCreateRequestModel::class,
            $userBudgetDemandCreateRequestModel
        );
        $userBudgetDemandCreateRequestModelResponse = $this->whenGetUserBudgetDemandCreateIsCalled($this->requestMock);
        $this->thenGetUserBudgetDemandCreateHasBeenCreated($content, $userBudgetDemandCreateRequestModelResponse);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function getUserBudgetDemandCreateShouldThrowException()
    {
        $this->givenRequestContentThrowsExceptionStepIsConfigured();
        $this->whenGetUserBudgetDemandCreateIsCalled($this->requestMock);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\RequestDataProvider::invalidDeserialization
     * @expectedException \Exception
     */
    public function serializationShouldThrowExcetionDueToBadClass($request, string $class)
    {
        $this->whenDeserializeThenThrowExceptionDueToBadClass($request, $class);
    }

    private function givenRequestContentIsSetStepIsConfigured(string $content)
    {
        $this->requestMock
                ->expects($this->once())
                ->method('getContent')
                ->willReturn($content);
    }

    private function givenRequestContentThrowsExceptionStepIsConfigured()
    {
        $this->requestMock
                ->expects($this->once())
                ->method('getContent')
                ->will($this->throwException(new \Exception));
    }

    private function andGetModelStepIsConfigured(
        string $content,
        string $clazz,
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModel
    )
    {
        $this->serializerInterfaceMock
                ->expects($this->once())
                ->method('deserialize')
                ->with(
                    $content,
                    $clazz,
                    'json',
                    array('allow_extra_attributes' => false)
                )
                ->willReturn($userBudgetDemandCreateRequestModel);
    }

    private function whenDeserializeThenThrowExceptionDueToBadClass(
        $request,
        string $clazz
    )
    {
        if($clazz !== get_class($request)) {
            throw new \Exception();
        }
    }

    private function whenGetUserBudgetDemandCreateIsCalled(Request $request)
    {
        return $this->requestService->getUserBudgetDemandCreate($request);
    }

    private function thenGetUserBudgetDemandCreateHasBeenCreated(
        string $content,
        UserBudgetDemandCreateRequestModel $userBudgetDemandCreateRequestModelResponse)
    {
       $obj = json_decode($content);
       $this->assertThat($obj->telephone, $this->equalTo($userBudgetDemandCreateRequestModelResponse->getTelephone()));
    }

}
