<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

use \App\Model\BudgetDemandListRequestModel;

use \App\Service\RequestService;

class RequestServiceBudgetDemandListTest extends TestCase
{

    private $serializerInterfaceMock;
    private $requestMock;

    /** before */
    public function setUp()
    {
        $this->serializerInterfaceMock = $this->createMock(SerializerInterface::class);
        $this->requestMock = $this->createMock(Request::class);

        $this->requestService = new RequestService(
            $this->serializerInterfaceMock
        );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\RequestDataProvider::validBudgetDemandListRequestModelEmailSet
     */
    public function getBudgetDemandListShouldReturnBudgetDemandListRequestModel(
        BudgetDemandListRequestModel $budgetDemandListRequestModel,
        string $content
    )
    {
        $this->givenRequestGetEmailIsSetStepIsConfigured($budgetDemandListRequestModel);
        $this->andGetModelStepIsConfigured(
            $content,
            BudgetDemandListRequestModel::class,
            $budgetDemandListRequestModel
        );
        $budgetDemandListRequestModelResponse = $this->whenGetBudgetDemandListIsCalled($this->requestMock);
        $this->thenGetBudgetDemandListWithEmailSetHasBeenListed($content, $budgetDemandListRequestModelResponse);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\RequestDataProvider::validBudgetDemandListRequestModelEmailNotSet
     */
    public function getBudgetDemandListShouldReturnNull(
        BudgetDemandListRequestModel $budgetDemandListRequestModel,
        string $content
    )
    {
        $this->givenRequestGetEmailIsNotSet();
        $budgetDemandListRequestModelResponse = $this->whenGetBudgetDemandListIsCalled($this->requestMock);
        $this->thenGetBudgetDemandListWithEmailNotSetHasBeenListed($content, $budgetDemandListRequestModelResponse);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\RequestDataProvider::invalidDeserialization
     * @expectedException \Exception
     */
    public function serializationShouldThrowExcetionDueToBadClass($request, string $class)
    {
        $this->whenDeserializeThenThrowExceptionDueToBadClass($request, $class);
    }

    private function givenRequestGetEmailIsSetStepIsConfigured(BudgetDemandListRequestModel $budgetDemandListRequestModel)
    {
        $this->requestMockConfig($budgetDemandListRequestModel->getEmail());
    }

    private function givenRequestGetEmailIsNotSet()
    {
        $this->requestMockConfig(null);
    }

    private function requestMockConfig($returnObject)
    {
        $this->requestMock
                ->expects($this->once())
                ->method('get')
                ->with('email')
                ->willReturn($returnObject);
    }

    private function whenDeserializeThenThrowExceptionDueToBadClass(
        $request,
        string $clazz
    )
    {
        if($clazz !== get_class($request)) {
            throw new \Exception();
        }
    }

    private function andGetModelStepIsConfigured(
        string $content,
        string $clazz,
        BudgetDemandListRequestModel $budgetDemandListRequestModel
    )
    {
        $this->serializerInterfaceMock
                ->expects($this->once())
                ->method('deserialize')
                ->with(
                    $content,
                    $clazz,
                    'json',
                    array('allow_extra_attributes' => false)
                )
                ->willReturn($budgetDemandListRequestModel);
    }

    private function whenGetBudgetDemandListIsCalled(Request $request): ?BudgetDemandListRequestModel
    {
        return $this->requestService->getBudgetDemandList($request);
    }

    private function thenGetBudgetDemandListWithEmailSetHasBeenListed(
        string $content,
        BudgetDemandListRequestModel $budgetDemandListRequestModel)
    {
       $obj = json_decode($content);
       $this->assertNotNull($budgetDemandListRequestModel->getEmail());
       $this->assertThat($obj->email, $this->equalTo($budgetDemandListRequestModel->getEmail()));
    }

    private function thenGetBudgetDemandListWithEmailNotSetHasBeenListed(
        string $content,
        ?BudgetDemandListRequestModel $budgetDemandListRequestModel)
    {
       $this->assertNull($budgetDemandListRequestModel);
    }

}
