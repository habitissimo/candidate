<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

use \App\Model\UserResponseModel;
use \App\Model\BudgetDemandResponseModel;
use \App\Service\ResponseService;

class ResponseServiceTest extends TestCase
{

    private const SERIALIZATION_ERROR_DEFAULT = "Serialization Error";

    private $serializerInterfaceMock;
    private $responseMock;

    /** before */
    public function setUp()
    {
        $this->serializerInterfaceMock = $this->createMock(SerializerInterface::class);
        $this->responseMock = $this->createMock(Response::class);

        $this->responseService = new ResponseService(
            $this->serializerInterfaceMock
        );
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\ResponseDataProvider::validUserResponseModel
     */
    public function getResponseShouldSuccessUserResponseModel(
        UserResponseModel $userResponseModel,
        $userResponseModelSerializedHelper
    )
    {
        $this->givenSerializedWithUserResponseModelStepIsConfigured($userResponseModel, $userResponseModelSerializedHelper);
        $response = $this->whenGetResponseIsCalled($userResponseModel);
        $this->thenGetResponseHasBeenReturned($response, $userResponseModelSerializedHelper);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\ResponseDataProvider::validBudgetDemandResponseModel
     */
    public function getResponseShouldSuccessBudgetDemandResponseModel(
        BudgetDemandResponseModel $budgetDemandResponseModel,
        $budgetDemandResponseModelSerializedHelper
    )
    {
        $this->givenSerializedWithBudgetDemandResponseModelStepIsConfigred($budgetDemandResponseModel, $budgetDemandResponseModelSerializedHelper);
        $response = $this->whenGetResponseIsCalled($budgetDemandResponseModel);
        $this->thenGetResponseHasBeenReturned($response, $budgetDemandResponseModelSerializedHelper);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\ResponseDataProvider::invalidObj
     */
    public function getResponseShouldFailsDueToMalformed($obj)
    {
        $this->givenSerializedFailsStepIsConfigured($obj);
        $response = $this->whenGetResponseIsCalled($obj);
        $this->thenGetResponseReturnIsEmpty($response);
    }

    private function givenSerializedWithUserResponseModelStepIsConfigured(
        UserResponseModel $userResponseModel,
        $userResponseModelSerializedHelper
    )
    {
        $this->serializerInterfaceMock
                ->expects($this->once())
                ->method('serialize')
                ->with($userResponseModel,'json')
                ->willReturn($userResponseModelSerializedHelper);
    }

    private function givenSerializedWithBudgetDemandResponseModelStepIsConfigred(
        BudgetDemandResponseModel $budgetDemandResponseModel,
        $budgetDemandResponseModelSerializedHelper)
    {
        $this->serializerInterfaceMock
                ->expects($this->once())
                ->method('serialize')
                ->with($budgetDemandResponseModel,'json')
                ->willReturn($budgetDemandResponseModelSerializedHelper);
    }

    private function givenSerializedFailsStepIsConfigured($obj)
    {
        $this->serializerInterfaceMock
                ->expects($this->once())
                ->method('serialize')
                ->with($obj,'json')
                ->willReturn(self::SERIALIZATION_ERROR_DEFAULT);
    }

    private function whenGetResponseIsCalled($userResponseModel)
    {
        return $this->responseService->getResponse($userResponseModel);
    }

    private function thenGetResponseHasBeenReturned(Response $response, string $userResponseModelSerializedHelper)
    {
       $this->assertThat($userResponseModelSerializedHelper, $this->equalTo($response->getContent()));
    }

    private function thenGetResponseReturnIsEmpty(Response $response)
    {
       $this->assertThat(self::SERIALIZATION_ERROR_DEFAULT, $this->equalTo($response->getContent()));
    }

}
