<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Entity\BudgetDemand;

class BudgetDemandTest extends TestCase
{

    private $reflectionClass;
    
    /** before */
    public function setUp()
    {
        $this->reflectionClass = new \ReflectionClass(BudgetDemand::class);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\BudgetDemandDataProvider::validBudgetDemandEntityMethods
    */
    public function methodsShouldBePresent(string $functionName)
    {
        $this->assertThat(true, $this->equalTo($this->reflectionClass->hasMethod($functionName)));
    }

    /**
     * @test
    */
    public function numberOfMethodsAllowedIs11()
    {
        $this->assertThat(11, $this->equalTo(count($this->reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC))));
    }

}
