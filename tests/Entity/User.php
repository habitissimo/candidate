<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Entity\User;

class UserTest extends TestCase
{

    private $reflectionClass;
    
    /** before */
    public function setUp()
    {
        $this->reflectionClass = new \ReflectionClass(User::class);
    }

    /**
     * @test
     * @dataProvider \App\Tests\Providers\UserDataProvider::validBudgetDemandEntityMethods
    */
    public function methodsShouldBePresent(string $functionName)
    {
        $this->assertThat(true, $this->equalTo($this->reflectionClass->hasMethod($functionName)));
    }

    /**
     * @test
    */
    public function numberOfMethodsAllowedIs7()
    {
        $this->assertThat(7, $this->equalTo(count($this->reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC))));
    }

}
